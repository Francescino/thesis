package it.polito.wifidata.datafiltering;

import it.polito.wifidata.datafiltering.model.Device;
import it.polito.wifidata.datafiltering.model.Variation;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Predicate;


/**
 * thread that given a ref to a list of devices determines which are not useful
 * and produces a list of index that have to be ignored once it terminates
 */
public class FilterJob implements Callable<List<Variation>> {
    private List<Device> nonFilteredDevices;
    private List<Variation> variations;
    private Long firstValidTimestamp;

    public FilterJob(List<Device> devices) {
        this.nonFilteredDevices = devices;
    }

    public List<Variation> call() {
        // sort devices
        nonFilteredDevices.sort(new Comparator<Device>() {
            @Override
            public int compare(Device o1, Device o2) {
                String firstObject = o1.getMacAddress();
                String secondObject = o2.getMacAddress();
                int compare = secondObject.compareTo(firstObject);
                if (compare == 0) {
                    return (int) (o1.getStartTime() - o2.getStartTime());
                }
                return compare;
            }
        });

        nonFilteredDevices.removeIf(new Predicate<Device>() {
            @Override
            public boolean test(Device device) {
                return device.getStartTime() < firstValidTimestamp;
            }
        });

        nonFilteredDevices = DeviceUtility.filterCameras(nonFilteredDevices);
        if (nonFilteredDevices != null && !nonFilteredDevices.isEmpty()) {
            // calculate events of ascend and descend
            List<Device> currentGroup = new ArrayList<>();
            Device cur;
            String currentMacAddress = nonFilteredDevices.get(0).getMacAddress();
            currentGroup.add(nonFilteredDevices.get(0));
            for (int i = 1; i < nonFilteredDevices.size(); i++) {
                cur = nonFilteredDevices.get(i);
                if (cur.getMacAddress().equals(currentMacAddress))
                    currentGroup.add(cur);
                else {
                    consumeGroup(currentGroup);
                    currentGroup.clear();
                    currentMacAddress = cur.getMacAddress();
                    currentGroup.add(cur);
                }
            }
            if (variations != null && !variations.isEmpty()) {
                variations.sort(new Comparator<Variation>() {
                    @Override
                    public int compare(Variation o1, Variation o2) {
                        return (int) (o1.getTimestamp() - o2.getTimestamp());
                    }
                });
                variations.removeIf(new Predicate<Variation>() {
                    @Override
                    public boolean test(Variation variation) {
                        return variation.getTimestamp() < firstValidTimestamp; // to remove bug where some invalid timestamp get into devices records
                    }
                });
            }
        }
        return variations;
    }


    private void consumeGroup(List<Device> currentGroup) {
        Long curLast = null;
        Long start = null;
        if (variations == null)
            variations = new ArrayList<>();
        for (int i = 0; i < currentGroup.size(); i++) {
            if (curLast == null) {
                start = currentGroup.get(i).getStartTime();
                curLast = currentGroup.get(i).getEndTime();
                continue;
            }
            if (Math.abs(curLast - currentGroup.get(i).getStartTime()) < 20 * 60 * 1000)
                curLast = currentGroup.get(i).getEndTime();
            else {
                start = null;
                curLast = null;
                i--;
            }
        }
        // check if a variations has been missed
        if (curLast != null && start != null &&
                curLast - start > 10 * 1000) {
            variations.add(new Variation(start,
                    false));
            variations.add(new Variation(curLast,
                    true));
        }
    }

    public List<Variation> getAscendAndDescends() {
        return variations;
    }

    public List<Device> getNonFilteredDevices() {
        return nonFilteredDevices;
    }

    public void setFirstValidTimestamp(Long timestamp) {
        firstValidTimestamp = timestamp;
    }
}
