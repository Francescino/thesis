package it.polito.wifidata.datafiltering;

import it.polito.wifidata.datafiltering.model.Device;
import it.polito.wifidata.datafiltering.model.Position;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import java.util.List;

public interface MobyService {

    @GET("collect_data/devices")
    Call<List<Device>> getDevices(@Query("router") String routerId,
                                  @Query("from") Long start,
                                  @Query("to") Long end);

    @GET("collect_data/positions")
    Call<List<Position>> getPositions(@Query("from") Long start,
                                      @Query("to") Long end,
                                      @Query("nPage") Integer pageNumber,
                                      @Query("nItem") Integer numItems);
}
