package it.polito.wifidata.datafiltering.busstopsapi.pojo;

public class LogicalStop {
    private Integer Id;
    private String Name;
    private Integer TypePoint;
    private Locality Locality;
    private Double Longitude;
    private Double Latitude;
    private Double Distance;
    private Object PostalCode;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Integer getTypePoint() {
        return TypePoint;
    }

    public void setTypePoint(Integer typePoint) {
        TypePoint = typePoint;
    }

    public it.polito.wifidata.datafiltering.busstopsapi.pojo.Locality getLocality() {
        return Locality;
    }

    public void setLocality(it.polito.wifidata.datafiltering.busstopsapi.pojo.Locality locality) {
        Locality = locality;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        Longitude = longitude;
    }

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }

    public Double getDistance() {
        return Distance;
    }

    public void setDistance(Double distance) {
        Distance = distance;
    }

    public Object getPostalCode() {
        return PostalCode;
    }

    public void setPostalCode(Object postalCode) {
        PostalCode = postalCode;
    }
}
