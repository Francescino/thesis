package it.polito.wifidata.datafiltering.busstopsapi.pojo;

import java.util.List;

public class Geometry {
    List<GeometryData> geometryData;

    public List<GeometryData> getGeometryData() {
        return geometryData;
    }

    public void setGeometryData(List<GeometryData> geometryData) {
        this.geometryData = geometryData;
    }

}
