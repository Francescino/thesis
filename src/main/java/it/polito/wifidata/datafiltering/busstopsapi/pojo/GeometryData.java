package it.polito.wifidata.datafiltering.busstopsapi.pojo;

import java.util.List;

public class GeometryData {
    public String type;
    public List<List<Double>> coordinates;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<List<Double>> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<List<Double>> coordinates) {
        this.coordinates = coordinates;
    }
}