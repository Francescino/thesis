package it.polito.wifidata.datafiltering.busstopsapi.pojo;

import it.polito.wifidata.datafiltering.model.GeoPoint;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class BusLineData {

    private List<Itinerary> Itineraries;
    private Integer LineId;
    private TransportMode TransportMode;
    private List<Stop> Stops;
    private boolean stopsOrdered = false;

    public Integer getNumberOfStopsBetween(Integer itineraryId, Integer stopId1, Integer stopId2) {
        Itinerary itinerary = getItinerary(itineraryId);
        if (stopId1.equals(stopId2)) return 0;
        if (itinerary != null) {
            int diff = 1;
            for (int i = 0; i < itinerary.getStopPointIds().size() - 1; i++) {
                if (itinerary.getStopPointIds().get(i).equals(stopId1)) {
                    boolean found = false;
                    for (int j = i + 1; j < itinerary.getStopPointIds().size(); j++, diff++) {
                        if (itinerary.getStopPointIds().get(j).equals(stopId2)) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        diff = -1;
                        for (int j = i - 1; j >= 0; j--, diff--) {
                            if (itinerary.getStopPointIds().get(j).equals(stopId2)) {
                                found = true;
                                break;
                            }
                        }
                    }
                    if (!found) return null;
                    return diff;
                }
            }
        }
        return null;
    }

    private Stop getStop(Integer stopId) {
        if (!stopsOrdered) {
            Stops.sort(new Comparator<Stop>() {
                @Override
                public int compare(Stop o1, Stop o2) {
                    return o1.getId() - o2.getId();
                }
            });
            stopsOrdered = true;
            return getStop(stopId);
        }
        int s, m, e;
        s = 0;
        e = Stops.size() - 1;
        while (s != e && s < Stops.size() && e >= 0) {
            m = (s + e) / 2;
            if (Stops.get(s).getId().equals(stopId))
                return Stops.get(s);
            if (Stops.get(e).getId().equals(stopId))
                return Stops.get(e);
            if (Stops.get(m).getId().equals(stopId))
                return Stops.get(m);
            if (Stops.get(m).getId() < stopId)
                s = m + 1;
            else
                e = m - 1;
        }
        return null;
    }

    public Stop getPreviousStop(int itineraryId, int stopId) {
        Itinerary itinerary = getItinerary(itineraryId);
        if (itinerary != null) {
            Integer previusStopId = null;
            for (int i = 1; i < itinerary.getStopPointIds().size(); i++) {
                if (itinerary.getStopPointIds().get(i) == stopId) {
                    previusStopId = itinerary.getStopPointIds()
                            .get(i - 1);
                    break;
                }
            }
            if (previusStopId != null)
                return getStop(previusStopId);
        }
        return null;
    }

    public Stop getNextStop(int itineraryId, int stopId) {
        Itinerary itinerary = getItinerary(itineraryId);
        if (itinerary != null) {
            Integer nextStopId = null;
            for (int i = 0; i < itinerary.getStopPointIds().size() - 1; i++) {
                if (itinerary.getStopPointIds().get(i) == stopId) {
                    nextStopId = itinerary.getStopPointIds()
                            .get(i + 1);
                    break;
                }
            }
            if (nextStopId != null)
                return getStop(nextStopId);

        }
        return null;
    }

    public List<Stop> getStopsByIds(List<Integer> stopIds) {
        List<Stop> res = new ArrayList<>();
        for (Stop stop : getStops()) {
            if (stopIds.contains(stop.getId()))
                res.add(stop);
        }
        return res;
    }

    public Stop getNearestStop(GeoPoint point, Integer itineraryId, Double matchDistance) {
        Stop res = null;
        Itinerary itinerary = getItinerary(itineraryId);
        if (itinerary != null) {
            Double minDistance = null;
            GeoPoint stopCoordinates = new GeoPoint(0., 0.);
            Double distance;
            List<Stop> stopsByIds = getStopsByIds(itinerary.getStopPointIds());
            for (Stop stopsById : stopsByIds) {
                stopCoordinates.setLat(stopsById.getLatitude());
                stopCoordinates.setLng(stopsById.getLongitude());
                distance = stopCoordinates.harvesineDistance(point);
                if (distance < matchDistance && (minDistance == null || distance < minDistance)) {
                    res = stopsById;
                    minDistance = distance;
                }
            }
        }
        return res;
    }


   /* public List[] intersections(List<Measure> measures)
    {
        Integer numberOfInterpolationPoints = 10;
        Double matchDistance = 30.;
        List[] partialResult = new List[getItineraries().size()];
        Itinerary itinerary;
        int moreThanZeroMatchCount = 0;
        for (int itineraryIndex = 0; itineraryIndex < getItineraries().size(); itineraryIndex++)
        {
            partialResult[itineraryIndex] = new ArrayList();
            itinerary = getItineraries().get(itineraryIndex);
            for (int measureIndex = 0; measureIndex < measures.size(); measureIndex++) {
                coordinateLoop:
                for (int geometryIndex = 0; geometryIndex < itinerary.getGeometries().size(); geometryIndex++) {
                    for (int coordinateIndex = 0; coordinateIndex < itinerary.getGeometries().get(geometryIndex).getGeometry().get(0).getCoordinates().size() - 1; coordinateIndex++) {
                        List<Double> first, second;
                        first = itinerary.getGeometries().get(geometryIndex).getGeometry().get(0).getCoordinates().get(coordinateIndex);
                        second = itinerary.getGeometries().get(geometryIndex).getGeometry().get(0).getCoordinates().get(coordinateIndex + 1);
                        GeoInterpolator geoInterpolator = new GeoInterpolator(first.get(1),
                                first.get(0),
                                second.get(1),
                                second.get(0),
                                numberOfInterpolationPoints);
                        Double distance;
                        while (geoInterpolator.hasNext()) {
                            GeoPoint itineraryPoint = geoInterpolator.next();
                            distance = itineraryPoint.harvesineDistance(measures.get(measureIndex).getPosition());
                            if (distance < matchDistance) {
                                partialResult[itineraryIndex].add(new Intersection(distance,
                                        itinerary.getStopPointIds().get(geometryIndex),
                                        itinerary.getStopPointIds().get(geometryIndex + 1),
                                        getLineId(),
                                        itinerary.getId(),
                                        measures.get(measureIndex),
                                        itineraryPoint));
                                break coordinateLoop;
                            }
                        }
                    }
                }
            }
            if (partialResult[itineraryIndex].size() > 0)
                moreThanZeroMatchCount++; // update counter of actual itineraries that got intersection for final result
        }
        if (moreThanZeroMatchCount == 0)
            return null;
        List[] result = new List[moreThanZeroMatchCount];
        int lastIndex = 0;
        for (List list : partialResult) {
            if (list.size() > 0)
            {
                result[lastIndex++] = list;
            }
        }
        Arrays.sort(result, new Comparator<List>() {
            @Override
            public int compare(List o1, List o2) {
                Intersection first = (Intersection) o1.get(0), second = (Intersection) o2.get(0);
                boolean firstLinearity, secondLinearity;
                firstLinearity = Intersection.isLinear(o1, getItinerary(first.getItineraryId()));
                secondLinearity = Intersection.isLinear(o2, getItinerary(second.getItineraryId()));
                if (firstLinearity && secondLinearity)
                {
                    // compare them by size
                    int diff = o1.size() - o2.size();
                    if (diff == 0) // compare them by sum of match distances
                    {
                        int firstSum, secondSum;
                        firstSum = secondSum = 0;
                        for (Object o : o1) {
                            Intersection intersection = (Intersection) o;
                            firstSum += intersection.getMatchDistance();
                        }

                        for (Object o : o2) {
                            Intersection intersection = (Intersection) o;
                            secondSum += intersection.getMatchDistance();
                        }
                        return firstSum - secondSum;
                    }
                }
                else if (firstLinearity)
                    return -1;
                else
                    return 1;

                return 0;
            }
        });

        return result;
    }*/

    public Itinerary getItinerary(Integer id) {
        for (Itinerary itinerary : getItineraries()) {
            if (itinerary.getId().equals(id))
                return itinerary;
        }
        return null;
    }

    public List<Itinerary> getItineraries() {
        return Itineraries;
    }

    public void setItineraries(List<Itinerary> itineraries) {
        Itineraries = itineraries;
    }

    public Integer getLineId() {
        return LineId;
    }

    public void setLineId(Integer lineId) {
        LineId = lineId;
    }

    public it.polito.wifidata.datafiltering.busstopsapi.pojo.TransportMode getTransportMode() {
        return TransportMode;
    }

    public void setTransportMode(it.polito.wifidata.datafiltering.busstopsapi.pojo.TransportMode transportMode) {
        TransportMode = transportMode;
    }

    public List<Stop> getStops() {
        return Stops;
    }

    public void setStops(List<Stop> stops) {
        Stops = stops;
    }

    public List<Itinerary> getItineraries(List<Integer> bestItineraries) {
        List<Itinerary> res = new ArrayList<>(bestItineraries.size());
        for (Itinerary itinerary : getItineraries()) {
            if (bestItineraries.contains(itinerary.getId())) {
                res.add(itinerary);
            }
        }
        return res;
    }
}
