package it.polito.wifidata.datafiltering.busstopsapi;

import it.polito.wifidata.datafiltering.busstopsapi.pojo.Response;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BusPathService {

    @GET("api/map/GetLineMap")
    Call<Response> getBusLine(@Query("lineId") String lineId);
}
