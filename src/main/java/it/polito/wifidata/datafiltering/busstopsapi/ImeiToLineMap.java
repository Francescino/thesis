package it.polito.wifidata.datafiltering.busstopsapi;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ImeiToLineMap {
    /**
     * Reads a file with filename equal to field filename
     * and reads a JSON File containing all lines that can be retrieved
     * and which a map between imei and which line that imei could serve
     */

    private static final String filename = "lineMapping.json";
    private static final String defaultLinesIdField = "defaultLinesId";

    public static String[] getLinesId(String imei) {
        Gson gson = new Gson();
        String[] result = null;
        try (JsonReader reader = new JsonReader(new FileReader(filename))) {
            JsonObject map = gson.fromJson(reader, JsonObject.class);
            JsonArray selectedElement = map.has(imei) ? map.getAsJsonArray(imei) : map.getAsJsonArray(defaultLinesIdField);
            int index = 0;
            result = new String[selectedElement.size()];
            for (JsonElement jsonElement : selectedElement) {
                result[index++] = jsonElement.getAsString();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }


}
