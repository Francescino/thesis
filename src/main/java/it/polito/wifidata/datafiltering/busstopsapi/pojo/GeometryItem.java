package it.polito.wifidata.datafiltering.busstopsapi.pojo;

import java.util.List;

public class GeometryItem {
    private Departure Departure;
    private Arrival Arrival;
    private Object Label;
    private Object Style;
    private List<GeometryData> Geometry;

    public it.polito.wifidata.datafiltering.busstopsapi.pojo.Departure getDeparture() {
        return Departure;
    }

    public void setDeparture(it.polito.wifidata.datafiltering.busstopsapi.pojo.Departure departure) {
        Departure = departure;
    }

    public it.polito.wifidata.datafiltering.busstopsapi.pojo.Arrival getArrival() {
        return Arrival;
    }

    public void setArrival(it.polito.wifidata.datafiltering.busstopsapi.pojo.Arrival arrival) {
        Arrival = arrival;
    }

    public Object getLabel() {
        return Label;
    }

    public void setLabel(Object label) {
        Label = label;
    }

    public Object getStyle() {
        return Style;
    }

    public void setStyle(Object style) {
        Style = style;
    }

    public List<GeometryData> getGeometry() {
        return Geometry;
    }

    public void setGeometry(List<GeometryData> geometry) {
        Geometry = geometry;
    }
}
