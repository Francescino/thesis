package it.polito.wifidata.datafiltering.busstopsapi.pojo;

import java.util.List;

public class Arrival {
    private Double Latitude;
    private Double Longitude;
    private List<Double> Value;
    private List<Double> GeoJsonValue;

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        Longitude = longitude;
    }

    public List<Double> getValue() {
        return Value;
    }

    public void setValue(List<Double> value) {
        Value = value;
    }

    public List<Double> getGeoJsonValue() {
        return GeoJsonValue;
    }

    public void setGeoJsonValue(List<Double> geoJsonValue) {
        GeoJsonValue = geoJsonValue;
    }
}
