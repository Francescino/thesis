package it.polito.wifidata.datafiltering.busstopsapi.pojo;

public class Response {

    private BusLineData Data;
    private Message Message;
    private Integer Status;

    public BusLineData getBusData() {
        return Data;
    }

    public void setBusData(BusLineData busLineData) {
        Data = busLineData;
    }

    public it.polito.wifidata.datafiltering.busstopsapi.pojo.Message getMessage() {
        return Message;
    }

    public void setMessage(it.polito.wifidata.datafiltering.busstopsapi.pojo.Message message) {
        Message = message;
    }

    public Integer getStatus() {
        return Status;
    }

    public void setStatus(Integer status) {
        Status = status;
    }
}
