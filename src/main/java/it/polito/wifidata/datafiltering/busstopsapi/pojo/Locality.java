package it.polito.wifidata.datafiltering.busstopsapi.pojo;

public class Locality {
    private Integer Id;
    private String Name;
    private String Code;
    private Double Longitude;
    private Double Latitude;
    private Object FareZone;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        Longitude = longitude;
    }

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }

    public Object getFareZone() {
        return FareZone;
    }

    public void setFareZone(Object fareZone) {
        FareZone = fareZone;
    }
}
