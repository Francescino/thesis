package it.polito.wifidata.datafiltering.busstopsapi;


import it.polito.wifidata.datafiltering.busstopsapi.pojo.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;

public class Main {

    static String baseUrl = "https://www.oise-mobilite.fr/";

    public static void main(String[] args) throws IOException {
        /*CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("https://www.oise-mobilite.fr/api/map/GetLineMap?lineId=256");
        httpGet.setHeader("Accept","application/json");
        CloseableHttpResponse response1 = httpclient.execute(httpGet);
        try {
            HttpEntity entity= response1.getEntity();
            //entity.writeTo(System.out);
            ObjectMapper om=new ObjectMapper();
            om.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
            MainEnvelope e=om.readValue(entity.getContent(), MainEnvelope.class);
            System.out.println(e.toString());
        } finally {
            response1.close();
        }*/
        Retrofit retrofit = retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        BusPathService busPathService = retrofit.create(BusPathService.class);
        retrofit2.Response<Response> response = busPathService.getBusLine("256").execute();
        it.polito.wifidata.datafiltering.busstopsapi.pojo.Response responseBusPath = response.body();

    }
}
