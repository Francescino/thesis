package it.polito.wifidata.datafiltering.busstopsapi.pojo;

public class Stop {
    private LogicalStop LogicalStop;
    private String Code;
    private Object Partner;
    private TransportMode TransportMode;
    private Integer Id;
    private String Name;
    private Integer TypePoint;
    private Locality Locality;
    private Double Longitude;
    private Double Latitude;
    private Object distance;
    private Object PostalCode;

    public LogicalStop getLogicalStop() {
        return LogicalStop;
    }

    public void setLogicalStop(LogicalStop logicalStop) {
        LogicalStop = logicalStop;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public Object getPartner() {
        return Partner;
    }

    public void setPartner(Object partner) {
        Partner = partner;
    }

    public it.polito.wifidata.datafiltering.busstopsapi.pojo.TransportMode getTransportMode() {
        return TransportMode;
    }

    public void setTransportMode(it.polito.wifidata.datafiltering.busstopsapi.pojo.TransportMode transportMode) {
        TransportMode = transportMode;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Integer getTypePoint() {
        return TypePoint;
    }

    public void setTypePoint(Integer typePoint) {
        TypePoint = typePoint;
    }

    public Locality getLocality() {
        return Locality;
    }

    public void setLocality(Locality locality) {
        Locality = locality;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        Longitude = longitude;
    }

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }

    public Object getDistance() {
        return distance;
    }

    public void setDistance(Object distance) {
        this.distance = distance;
    }

    public Object getPostalCode() {
        return PostalCode;
    }

    public void setPostalCode(Object postalCode) {
        PostalCode = postalCode;
    }
}
