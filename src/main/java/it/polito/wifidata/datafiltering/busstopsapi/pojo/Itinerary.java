package it.polito.wifidata.datafiltering.busstopsapi.pojo;

import java.util.List;

public class Itinerary {
    private Integer FirstStopId;
    private Integer LastStopId;
    private Integer Id;
    private Integer Direction;
    private Integer LineId;
    private List<Integer> StopPointIds;
    private List<GeometryItem> Geometries;

    public Integer getFirstStopId() {
        return FirstStopId;
    }

    public void setFirstStopId(Integer firstStopId) {
        FirstStopId = firstStopId;
    }

    public Integer getLastStopId() {
        return LastStopId;
    }

    public void setLastStopId(Integer lastStopId) {
        LastStopId = lastStopId;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Integer getDirection() {
        return Direction;
    }

    public void setDirection(Integer direction) {
        Direction = direction;
    }

    public Integer getLineId() {
        return LineId;
    }

    public void setLineId(Integer lineId) {
        LineId = lineId;
    }

    public List<Integer> getStopPointIds() {
        return StopPointIds;
    }

    public void setStopPointIds(List<Integer> stopPointIds) {
        StopPointIds = stopPointIds;
    }

    public List<GeometryItem> getGeometries() {
        return Geometries;
    }

    public void setGeometries(List<GeometryItem> geometries) {
        Geometries = geometries;
    }
}
