package it.polito.wifidata.datafiltering.busstopsapi;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import it.polito.wifidata.MongoClientSingleton;
import it.polito.wifidata.datafiltering.busstopsapi.pojo.BusLineData;
import it.polito.wifidata.datafiltering.busstopsapi.pojo.Itinerary;
import it.polito.wifidata.datafiltering.busstopsapi.pojo.Response;
import org.bson.Document;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public class LineManager {
    // TODO add locality id in the future
    private static final String BASE_URL = "https://www.oise-mobilite.fr/";
    private static final String COLLECTION_NAME = "routerIdToLineAssociation";
    private static final String LINE_ID_FIELD = "linesId";

    public static List<String> getLineIds(String routerId) {
        MongoClient mongoClient = MongoClientSingleton.getInstance();
        MongoDatabase lineMappingConnections = mongoClient.getDatabase(COLLECTION_NAME);
        MongoCollection<Document> lineIdsCollection = lineMappingConnections.getCollection(routerId);
        List<String> linesId = new ArrayList<>();
        for (Document document : lineIdsCollection.find()) {
            linesId = (ArrayList<String>) document.get(LINE_ID_FIELD);
        }
        return linesId;
    }

    public static List<String> getRegisteredRoutersId() {
        MongoClient mongoClient = MongoClientSingleton.getInstance();
        MongoDatabase lineMappingConnections = mongoClient.getDatabase(COLLECTION_NAME);
        List<String> res = new ArrayList<>();
        for (String listCollectionName : lineMappingConnections.listCollectionNames()) {
            res.add(listCollectionName);
        }
        return res;
    }

    public static List<BusLineData> getLinePaths(String routerId) {
        // use mongo to discover which lines are associated to routerId
        Retrofit retrofit = retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        BusPathService busPathService = retrofit.create(BusPathService.class);
        List<BusLineData> result = new ArrayList<>();
        List<String> linesId = LineManager.getLineIds(routerId);
        for (String lineId : linesId) {
            retrofit2.Response<Response> response = null;
            try {
                response = busPathService.getBusLine(lineId).execute();
                it.polito.wifidata.datafiltering.busstopsapi.pojo.Response responseBusPath = response.body();
                if (responseBusPath != null) {
                    BusLineData busData = responseBusPath.getBusData();
                    orderAndFilter(busData);
                    result.add(busData);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private static void orderAndFilter(BusLineData lineData) {
        // order by size
        lineData.getItineraries().sort(new Comparator<Itinerary>() {
            @Override
            public int compare(Itinerary o1, Itinerary o2) {
                return o2.getStopPointIds().size() - o1.getStopPointIds().size();
            }
        });
        // and do cleaning
        List<Integer> toRemove = new ArrayList<>();
        for (int i = 0; i < lineData.getItineraries().size() - 1; i++) {
            Itinerary itinerary1 = lineData.getItineraries().get(i);
            for (int j = i + 1; j < lineData.getItineraries().size(); j++) {
                Itinerary itinerary2 = lineData.getItineraries().get(j);
                if (itinerary1.getStopPointIds().size()
                        == itinerary2.getStopPointIds().size()) {
                    boolean diff = false;
                    for (int k = 0; k < itinerary1.getStopPointIds().size(); k++) {
                        if (!itinerary1.getStopPointIds()
                                .get(k)
                                .equals(itinerary2.getStopPointIds()
                                        .get(k))) {
                            diff = true;
                            break;
                        }
                    }
                    if (!diff) {
                        toRemove.add(itinerary2.getId());
                    }

                }
            }
        }
        lineData.getItineraries().removeIf(new Predicate<Itinerary>() {
            @Override
            public boolean test(Itinerary itinerary) {
                return toRemove.contains(itinerary.getId());
            }
        });
    }
}
