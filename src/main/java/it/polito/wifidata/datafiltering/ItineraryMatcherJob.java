package it.polito.wifidata.datafiltering;

import it.polito.wifidata.datafiltering.busstopsapi.pojo.BusLineData;
import it.polito.wifidata.datafiltering.busstopsapi.pojo.Itinerary;
import it.polito.wifidata.datafiltering.busstopsapi.pojo.Stop;
import it.polito.wifidata.datafiltering.model.DFunctionResult;
import it.polito.wifidata.datafiltering.model.LineProbability;
import it.polito.wifidata.datafiltering.model.SegmentClassification;
import it.polito.wifidata.datafiltering.model.TimePoint;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class ItineraryMatcherJob implements Callable<SegmentClassification> {

    private static final long WAIT_RESULT = 5;
    // this thread recieves a List of Pairs <Long, GeoPoint>
    // and then produces the best line in the segment and then forwards
    // the segment along with the line id to the stops Thread
    private final int segmentNumber;
    private final Future<SegmentClassification> precClassification;
    private final String routerId;
    private final List<TimePoint> currentSegment;
    private List<BusLineData> allPossiblePaths;
    private LineProbability lineProbability;
    private MongoDBItineraryMatcher mongoDBItineraryMatcher;

    public ItineraryMatcherJob(String routerId,
                               List<BusLineData> linePaths,
                               int segmentNumber,
                               List<TimePoint> currentSegment,
                               Future<SegmentClassification> previousClassification) {
        this.routerId = routerId;
        this.currentSegment = currentSegment;
        this.segmentNumber = segmentNumber;
        allPossiblePaths = linePaths;
        precClassification = previousClassification;
    }

    private void init() {
        // build line probability object
        mongoDBItineraryMatcher = new MongoDBItineraryMatcher();
        List<Integer> itineraryIdsAL = new ArrayList<>();
        List<Integer> lineIdsAL = new ArrayList<>();
        for (BusLineData allPossiblePath : allPossiblePaths) {
            for (Itinerary itinerary : allPossiblePath.getItineraries()) {
                lineIdsAL.add(itinerary.getLineId());
                itineraryIdsAL.add(itinerary.getId());
            }
        }
        lineProbability = new LineProbability(routerId,
                lineIdsAL,
                itineraryIdsAL);
        // try to recover last segment classification
        if (precClassification != null) {
            try {
                SegmentClassification segmentClassification = precClassification.get();
                if (segmentClassification != null &&
                        segmentClassification.getLastRelevatedLineItineraryIds() != null)
                    lineProbability.initialProbability(segmentClassification.getLastRelevatedLineItineraryIds()[0],
                            segmentClassification.getLastRelevatedLineItineraryIds()[1],
                            segmentClassification.getLastRelevatedLineItineraryProbability());
            } catch (InterruptedException | ExecutionException e) {
                //e.printStackTrace();
            }
        }
    }


    @Override
    public SegmentClassification call() {
        int itineraryIndex;
        double newProbability;
        init();
        List<Pair<Integer, Integer>> bestLines = new ArrayList<>();
        for (TimePoint timePoint : currentSegment) {
            setTimestamp(timePoint.getTimestamp());
            itineraryIndex = 0;
            // match it to bus line itineraries
            for (BusLineData linePath : allPossiblePaths) {
                for (Itinerary itinerary : linePath.getItineraries()) {
                    calculateProbability(linePath,
                            itinerary,
                            timePoint,
                            itineraryIndex);
                    // check if current coordinate near any stop
                    if (lineProbability.getProbability(itineraryIndex) > 0) {
                        checkStopSequence(linePath,
                                itinerary,
                                timePoint,
                                itineraryIndex);
                    }
                    itineraryIndex++;
                }
            }
            // take best probability and mark timestamp belonging to that line-itinerary
            int[] bestLineItineraryId = lineProbability.getMostProbableLineItineraryId();
            if (bestLineItineraryId[0] >= 0 && bestLineItineraryId[1] >= 0) {
                if (isTheOnlyLineMatched(bestLineItineraryId)) {
                    return buildSegmentClassification(
                            Util.getBusLine(allPossiblePaths, bestLineItineraryId[0]),
                            bestLineItineraryId[1]);
                }
                bestLines.add(new Pair<>(bestLineItineraryId[0], bestLineItineraryId[1]));
            }
        }

        Integer bestLineId = findBestLineId(bestLines);

        if (bestLineId != null) {
            // find all itineraries matched with that line
            List<Integer> bestItineraries = new ArrayList<>();
            for (Pair<Integer, Integer> bestLine : bestLines) {
                if (bestLine.getKey().equals(bestLineId) && !bestItineraries.contains(bestLine.getValue())) {
                    bestItineraries.add(bestLine.getValue());
                }
            }

            BusLineData selectedLine = Util.getBusLine(allPossiblePaths,
                    bestLineId);

            // now selects itinerary and get the one with more stops
            List<Itinerary> itineraries = null;
            if (selectedLine != null)
                itineraries = selectedLine.getItineraries(bestItineraries);
            if (itineraries != null && !itineraries.isEmpty())
                return buildSegmentClassification(selectedLine,
                        itineraries.get(0).getId());
        }
        return null;
    }

    private SegmentClassification buildSegmentClassification(BusLineData selectedLine,
                                                             Integer itineraryId) {
        return new SegmentClassification(routerId,
                currentSegment,
                segmentNumber,
                selectedLine,
                itineraryId,
                lineProbability.getMostProbableLineItineraryId(),
                lineProbability.getProbabilityByItineraryId(
                        lineProbability.getMostProbableLineItineraryId()[1]));
    }


    private Integer findBestLineId(List<Pair<Integer, Integer>> bestLines) {
        Integer bestLineId = null;
        if (bestLines.size() > 1) {
            Map<Integer, Integer> occurences = new HashMap<>();
            for (Pair<Integer, Integer> bestLine : bestLines) {
                Integer key = bestLine.getKey();
                //+ "-" +
                //Integer.toString(bestLine.getValue());
                occurences.put(key, occurences.containsKey(key) ?
                        occurences.get(key) + 1 : 0);
            }
            Integer maxFreq = null;
            for (Map.Entry<Integer, Integer> occurrence : occurences.entrySet()) {
                if (bestLineId == null || occurrence.getValue() > maxFreq) {
                    bestLineId = occurrence.getKey();
                    maxFreq = occurrence.getValue();
                }
            }
        } else if (!bestLines.isEmpty()) {
            bestLineId = bestLines.get(0).getKey();
        }
        return bestLineId;
    }

    private void setTimestamp(Long timestamp) {
        // if too much time is passed from last timestamp reset probability
        if (lineProbability.getTimestamp() != null &&
                Math.abs(timestamp - lineProbability.getTimestamp()) >
                        Config.getInstance()
                                .getLong("ITINERARY_MATCHER_RESET_TIME")) {
            // distance among timestamps too big reset probabilities
            lineProbability.reset();
        }
        lineProbability.setTimestamp(timestamp);
    }

    private void calculateProbability(BusLineData linePath,
                                      Itinerary itinerary,
                                      TimePoint timePoint,
                                      int itineraryIndex) {
        DFunctionResult dFunctionResult = mongoDBItineraryMatcher.DFunction(linePath,
                itinerary.getId(),
                timePoint,
                lineProbability.getIndex(itineraryIndex));
        double DFunctionAParameter = Config.getInstance()
                .getDouble("D_FUNCTION_A_PARAMETER");
        double newProbability = dFunctionResult == null ? 0 :
                DFunctionAParameter * lineProbability.getProbability(itineraryIndex) +
                        (1 - DFunctionAParameter) * (1000 / (999 + Math.exp(dFunctionResult.getDistance() / 3)));
        lineProbability.setProbability(itineraryIndex,
                newProbability,
                dFunctionResult == null ? 0 : dFunctionResult.getSeqNumber());
    }

    private void checkStopSequence(BusLineData linePath,
                                   Itinerary itinerary,
                                   TimePoint timePoint,
                                   int itineraryIndex) {
        Stop nearestStop =
                mongoDBItineraryMatcher.getNearestStop(linePath,
                        itinerary.getId(),
                        timePoint,
                        null);
        Stop previousExpectedStop, nextStop;
        if (nearestStop != null) {
            // check if the stop found is the next expected stop
            if ((nextStop = lineProbability.getNextExpectedStop(itineraryIndex)) == null ||
                    nearestStop.getId()
                            .equals(nextStop.getId())) {
                lineProbability.setNextExpectedStop(itineraryIndex,
                        linePath.getNextStop(itinerary.getId(),
                                nearestStop.getId()));
            }
            // otherwise check if it matches with previous stop of next stop
            // this is the case when the bus is still departing from previous stop
            else if ((previousExpectedStop = linePath.getPreviousStop(itinerary.getId(),
                    lineProbability.getNextExpectedStop(itineraryIndex).getId())) != null &&
                    !previousExpectedStop.getId().equals(nearestStop.getId())) {
                // if is not even the previous calculate the difference and move probability accordingly
                Integer numberOfStopsBetween = linePath.getNumberOfStopsBetween(itinerary.getId(),
                        previousExpectedStop.getId(),
                        nearestStop.getId());
                if (numberOfStopsBetween != null && numberOfStopsBetween > 0) {
                    double v = 1 / (0.5882 * numberOfStopsBetween) * lineProbability.getProbability(itineraryIndex);
                    lineProbability.setProbability(itineraryIndex, v, lineProbability.getIndex(itineraryIndex));
                    lineProbability.getNextExpectedStops()[itineraryIndex] = linePath.getNextStop(itinerary.getId(),
                            nearestStop.getId());
                } else {
                    lineProbability.getNextExpectedStops()[itineraryIndex] = null;
                    lineProbability.setProbability(itineraryIndex, 0., 0);
                }
            }
        }
    }

    public boolean isTheOnlyLineMatched(int[] bestLineItineraryId) {
        boolean moreThanOne = false;
        int bestIndex = -1;
        for (int i = 0; i < lineProbability.getItineraryIds().length; i++) {
            if (lineProbability.getLineIds()[i] == bestLineItineraryId[0]) {
                bestIndex = i;
                continue;
            }
            if (lineProbability.getProbability(i) > 0) {
                moreThanOne = true;
                break;
            }
        }
        // 1 high probability and all other at zero -> bus is on a path specific to only one itinerary
        return !moreThanOne &&
                bestIndex > 0 &&
                lineProbability.getProbability(bestIndex) > 0.85;
    }

}
