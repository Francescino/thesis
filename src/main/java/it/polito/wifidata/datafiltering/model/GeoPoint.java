package it.polito.wifidata.datafiltering.model;

public class GeoPoint {

    private Double lat;
    private Double lng;

    public GeoPoint() {
        this(0., 0.);
    }

    public GeoPoint(Double latitude, Double longitude) {
        this.lat = latitude;
        this.lng = longitude;
    }

    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    // distance in meters
    public double harvesineDistance(GeoPoint other) {

        Double lat1 = getLat();
        Double lat2 = other.getLat();

        Double lon1 = getLng();
        Double lon2 = other.getLng();

        double el1, el2;

        el1 = el2 = 0.;

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }


    @Override
    public String toString() {
        return Double.toString(lat) + " " + Double.toString(lng);
    }

    public String toStringComma() {
        return Double.toString(lat) + "," + Double.toString(lng);
    }
}
