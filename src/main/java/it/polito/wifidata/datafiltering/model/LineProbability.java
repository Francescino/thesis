package it.polito.wifidata.datafiltering.model;

import it.polito.wifidata.datafiltering.Config;
import it.polito.wifidata.datafiltering.busstopsapi.pojo.Stop;

import java.util.List;

public class LineProbability {
    private static final int INVALID_VALUE = -1;
    private static final double epsilon = 0.05;
    private Integer bestIndex;
    private String imei;
    private Long timestamp;
    private int[] lineIds;
    private int[] itineraryIds;
    private double[] probabilities;
    private int[] coordinateSequenceNumber;
    private Stop[] nextStops;

    public LineProbability(String imei,
                           List<Integer> lineIds,
                           List<Integer> itineraryIds) {
        int size = lineIds.size();
        this.imei = imei;
        this.lineIds = new int[size];
        this.itineraryIds = new int[itineraryIds.size()];
        for (int i = 0; i < this.lineIds.length; i++) {
            this.lineIds[i] = lineIds.get(i);
            this.itineraryIds[i] = itineraryIds.get(i);
        }
        this.probabilities = new double[size];
        this.coordinateSequenceNumber = new int[size];
        this.nextStops = new Stop[size];
        for (int i = 0; i < lineIds.size(); i++) {
            setProbability(i, 0, INVALID_VALUE);
        }
        bestIndex = null;
    }

    public LineProbability(String imei,
                           int[] lineIds,
                           int[] itineraryIds) {
        this.imei = imei;
        this.lineIds = lineIds;
        this.itineraryIds = itineraryIds;
        this.probabilities = new double[lineIds.length];
        this.coordinateSequenceNumber = new int[lineIds.length];
        nextStops = new Stop[lineIds.length];
        for (int i = 0; i < probabilities.length; i++) {
            coordinateSequenceNumber[i] = INVALID_VALUE;
        }
        bestIndex = null;
    }

    public int[] getMostProbableLineItineraryId() {
        int[] res = new int[2];
        res[0] = res[1] = INVALID_VALUE;
        if (bestIndex != null) {
            res[0] = lineIds[bestIndex];
            res[1] = itineraryIds[bestIndex];
        }
        return res;
    }

    public int[] getLineIds() {
        return lineIds;
    }

    public Stop getNextExpectedStop(int index) {
        if (index < 0 || index >= nextStops.length) return null;
        return nextStops[index];
    }

    public void setNextExpectedStop(int index, Stop stop) {
        if (index >= 0 && index < nextStops.length) {
            nextStops[index] = stop;
        }
    }

    public Stop[] getNextExpectedStops() {
        return nextStops;
    }

    public int[] getItineraryIds() {
        return itineraryIds;
    }

    public Double getProbability(int index) {
        return probabilities[index];
    }

    public Double getProbabilityByItineraryId(int itineraryId) {
        for (int i = 0; i < itineraryIds.length; i++) {
            if (itineraryIds[i] == itineraryId)
                return probabilities[i];
        }
        return null;
    }

    public void setProbability(int itineraryIndex, double newProbability, int newIndex) {
        probabilities[itineraryIndex] = newProbability < epsilon ? 0. : newProbability;
        coordinateSequenceNumber[itineraryIndex] = newProbability <= 0 ? INVALID_VALUE : newIndex;
        if (probabilities[itineraryIndex] >
                Config.getInstance()
                        .getDouble("LINE_PROBABILITY_THRESHOLD") && // put high threshold
                (bestIndex == null ||
                        (probabilities[itineraryIndex] > probabilities[bestIndex]))) {
            bestIndex = itineraryIndex;
        }
    }

    /*public void setProbability(int itineraryId, double newProbability, int newIndex) {
        for (int i = 0; i < itineraryIds.length; i++) {
            if (itineraryIds[i] == itineraryId) {
                probabilities[i] = newProbability < epsilon? 0. : newProbability;
                coordinateSequenceNumber[i] = newProbability < 0? INVALID_VALUE : newIndex;
                break;
            }
        }
    }*/

    public LineProbability clone() {
        LineProbability result = new LineProbability(imei,
                lineIds.clone(),
                itineraryIds.clone());
        result.timestamp = timestamp;
        result.coordinateSequenceNumber = coordinateSequenceNumber.clone();
        result.probabilities = probabilities.clone();
        return result;
    }


    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }


    public int getIndex(int index) {
        return coordinateSequenceNumber[index];
    }

    public int getIndex(Integer itineraryId) {
        for (int i = 0; i < itineraryIds.length; i++) {
            if (itineraryId.equals(itineraryIds[i])) {
                if (coordinateSequenceNumber[i] == INVALID_VALUE)
                    break;
                return coordinateSequenceNumber[i];
            }
        }
        return 0;
    }


    public void reset() {
        for (int i = 0; i < probabilities.length; i++) {
            probabilities[i] = 0;
        }
        for (int i = 0; i < coordinateSequenceNumber.length; i++) {
            coordinateSequenceNumber[i] = INVALID_VALUE;
        }
    }

    public void initialProbability(Integer precLineId,
                                   Integer precItineraryId,
                                   Double initialProbability) {
        if (precLineId < 0 || precItineraryId < 0 || initialProbability == null) return;
        for (int i = 0; i < lineIds.length; i++) {
            if (lineIds[i] == precLineId && itineraryIds[i] == precItineraryId) {
                probabilities[i] = initialProbability;
                break;
            }
        }
    }
}
