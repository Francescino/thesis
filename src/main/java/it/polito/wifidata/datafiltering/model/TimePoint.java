package it.polito.wifidata.datafiltering.model;

import java.util.Date;

public class TimePoint extends GeoPoint {

    private Long timestamp;

    public TimePoint(Double lat,
                     Double lon,
                     Long timestamp) {
        super(lat, lon);
        this.timestamp = timestamp;
    }

    public TimePoint(GeoPoint geoPoint,
                     Long timestamp) {
        this(geoPoint.getLat(),
                geoPoint.getLng(),
                timestamp);
    }

    @Override
    public String toString() {
        return new Date(timestamp) + " " + getLat() + " " + getLng();
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long val) {
        this.timestamp = val;
    }
}
