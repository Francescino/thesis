package it.polito.wifidata.datafiltering.model;

public class Relevation {
    private String imei;
    private Long timestamp;
    private int value;

    public Relevation(String imei, Long timestamp, boolean firstTimestamp) {
        this.imei = imei;
        this.timestamp = timestamp;
        setValue(firstTimestamp);
    }

    public Relevation(String imei, Long timestamp, int value) {
        this.imei = imei;
        this.timestamp = timestamp;
        this.value = value;
    }

    public boolean isStartingTimestamp() {
        return value == 1;
    }

    public void setValue(boolean firstTimestamp) {
        value = firstTimestamp ? 1 : -1;
    }

    @Override
    public String toString() {
        return "Relevation{" +
                "imei='" + imei + '\'' +
                ", timestamp=" + timestamp +
                ", value=" + value +
                '}';
    }
}
