package it.polito.wifidata.datafiltering.model;

import it.polito.wifidata.datafiltering.busstopsapi.pojo.Stop;
import sun.awt.Mutex;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public class StopsTable {

    private static StopsTable instance;
    private List<StopsTableEntry> entries;
    private Mutex mutex = new Mutex();

    private StopsTable() {
        entries = new ArrayList<>();
    }

    public static void newInstance() {
        instance = new StopsTable();
    }

    public static StopsTable getInstance() {
        return instance;
    }

    public List<StopsTableEntry> getEntries() {
        return entries;
    }

    public void setEntries(List<StopsTableEntry> entries) {
        this.entries = entries;
    }

    public void addEntry(String routerId,
                         Integer lineId,
                         Integer itineraryId,
                         Stop stop,
                         Long timestamp,
                         Double distanceStopTimestampPoint,
                         int segmentNumber) {
        mutex.lock();
        // search if stop already present and in a near instant
        for (int i = 0; i < entries.size(); i++) {
            StopsTableEntry stopsTableEntry = entries.get(i);
            if (routerId.equals(stopsTableEntry.getRouterId()) &&
                    lineId.equals(stopsTableEntry.getLineId()) &&
                    itineraryId.equals(stopsTableEntry.getItineraryId()) &&
                    stopsTableEntry.getStop().getId().equals(stop.getId())) {
                // stop collision occurred

                // check if timestamp is near
                if (Math.abs(stopsTableEntry.getTimestamp() - timestamp) < 5 * 60 * 1000) {
                    if (stopsTableEntry.getMatchDistance() > distanceStopTimestampPoint) {
                        stopsTableEntry.setTimestamp(timestamp);
                        stopsTableEntry.setMatchDistance(distanceStopTimestampPoint);
                        entries.set(i, stopsTableEntry);
                    }
                } else if (i < entries.size() - 1) {
                    continue;
                } else {
                    // if not then it must have reached it later
                    break;
                }
                mutex.unlock();
                return;
            }
        }
        // add entry
        entries.add(new StopsTableEntry(routerId,
                lineId,
                itineraryId,
                stop,
                timestamp,
                distanceStopTimestampPoint,
                segmentNumber));
        mutex.unlock();
    }


    public void sort() {
        entries.sort(new Comparator<StopsTableEntry>() {
            @Override
            public int compare(StopsTableEntry o1, StopsTableEntry o2) {
                return (int) (o1.getTimestamp() - o2.getTimestamp());
            }
        });
    }

    public void consumeAscendAndDescends(List<Variation> ascendAndDescends) {
        // since ascends and descends are ordered by timestamp
        // first eliminate stops before the first timestamp in the ascendAndDescends and after last timestamp
        // to avoid problems when no devices where relevated due to errors in the relevator
        Long firstTimestamp = ascendAndDescends.get(0).getTimestamp();
        Long lastTimestamp = ascendAndDescends.get(ascendAndDescends.size() - 1).getTimestamp();
        entries.removeIf(new Predicate<StopsTableEntry>() {
            @Override
            public boolean test(StopsTableEntry entry) {
                return entry.getTimestamp() < firstTimestamp ||
                        entry.getTimestamp() > lastTimestamp;
            }
        });
        // go through stops table step 2
        int currentStopsTableIndex = 0;
        long timestamp;
        for (Variation ascendAndDescend : ascendAndDescends) {
            timestamp = ascendAndDescend.getTimestamp();
            while (currentStopsTableIndex < entries.size() - 1 &&
                    timestamp > entries.get(currentStopsTableIndex + 1).getTimestamp()) currentStopsTableIndex++;
            if (currentStopsTableIndex >= entries.size() - 1) break;
            if (ascendAndDescend.isDescent()) {
                entries.get(currentStopsTableIndex + 1).descent();
            } else {
                entries.get(currentStopsTableIndex).ascent();
            }
        }
        // now compute total based on previous stop
        int sum = 0;
        if (!entries.isEmpty()) {
            entries.get(0).setNumOfDeviceOnBoard(0, null);
            for (int i = 1; i < entries.size(); i++) {
                entries.get(i).setNumOfDeviceOnBoard(entries.get(i - 1)
                                .getDevicesOnBoard(),
                        entries.get(i - 1)
                                .getTimestamp());
            }
        }
    }
}
