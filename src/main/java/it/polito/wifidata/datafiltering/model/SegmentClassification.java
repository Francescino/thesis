package it.polito.wifidata.datafiltering.model;

import it.polito.wifidata.datafiltering.busstopsapi.pojo.BusLineData;

import java.util.Date;
import java.util.List;

public class SegmentClassification {
    private final int segmentNumber;
    private final List<TimePoint> segment;
    private final Integer itineraryId;
    private final BusLineData lineData;
    private String routerId;
    private int[] lastRelevatedLineItineraryIds;
    private double lastRelevatedLineItineraryProbability;

    private SegmentClassification(String routerId,
                                  List<TimePoint> currentSegment,
                                  int segmentNumber,
                                  BusLineData selectedLine,
                                  Integer bestItineraryId) {
        this.routerId = routerId;
        this.segmentNumber = segmentNumber;
        this.segment = currentSegment;
        this.lineData = selectedLine;
        this.itineraryId = bestItineraryId;
    }

    public SegmentClassification(String routerId,
                                 List<TimePoint> currentSegment,
                                 int segmentNumber,
                                 BusLineData selectedLine,
                                 Integer id,
                                 int[] mostProbableLineItineraryId,
                                 double probability) {
        this(routerId, currentSegment, segmentNumber, selectedLine, id);
        lastRelevatedLineItineraryIds = mostProbableLineItineraryId;
        lastRelevatedLineItineraryProbability = probability;

    }

    public double getLastRelevatedLineItineraryProbability() {
        return lastRelevatedLineItineraryProbability;
    }

    public int[] getLastRelevatedLineItineraryIds() {
        return lastRelevatedLineItineraryIds;
    }

    public List<TimePoint> getSegment() {
        return segment;
    }

    public Integer getItineraryId() {
        return itineraryId;
    }

    public BusLineData getLineData() {
        return lineData;
    }

    public int getSegmentNumber() {
        return segmentNumber;
    }

    public String getRouterId() {
        return routerId;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("From ");
        stringBuilder.append(new Date(segment.get(0).getTimestamp()));
        stringBuilder.append(" ");
        stringBuilder.append(new Date(segment.get(segment.size() - 1).getTimestamp()));
        stringBuilder.append(" ");
        stringBuilder.append("Line: ");
        stringBuilder.append(lineData.getLineId());
        stringBuilder.append(", Itinerary:");
        stringBuilder.append(itineraryId);
        return stringBuilder.toString();
    }
}
