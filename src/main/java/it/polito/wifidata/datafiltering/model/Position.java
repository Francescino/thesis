package it.polito.wifidata.datafiltering.model;

import java.util.Date;

public class Position {

    private String imei;
    private Long timestamp;
    private Long date;
    private Double lng;
    private Double lat;
    private Double altitude;
    private Double angle;
    private Integer satellites;
    private Double speed;


    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    public Double getAngle() {
        return angle;
    }

    public void setAngle(Double angle) {
        this.angle = angle;
    }

    public Integer getSatellites() {
        return satellites;
    }

    public void setSatellites(Integer satellites) {
        this.satellites = satellites;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    @Override
    public String toString() {
        return "Position{" +
                "imei='" + imei + '\'' +
                ", timestamp=" + new Date(timestamp) +
                ", lng=" + lng +
                ", lat=" + lat +
                //              ", altitude=" + altitude +
                //              ", angle=" + angle +
                //             ", satellites=" + satellites +
                ", speed=" + speed +
                '}';
    }
}
