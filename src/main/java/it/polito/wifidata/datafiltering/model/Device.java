package it.polito.wifidata.datafiltering.model;

import java.util.List;
import java.util.stream.Collectors;

/**
 * class modelling raw data recieved from end point
 */
public class Device {
    private String _id;
    private int counter;
    private Long duration;
    private Long startTime;
    private Long endTime;
    private List<String> footprints;
    private String globalMacAddress;
    private List<String> localMacAddresses;
    private List<Long> timestamps;
    private List<GeoPoint> points;
    private String router;
    private List<Integer> rssis;
    private List<String> ssids;
    private boolean expired;

    public Device(Device device) {
        _id = device._id;
        counter = device.counter;
        duration = device.duration;
        startTime = device.startTime;
        endTime = device.endTime;
        footprints = device.footprints.stream().collect(Collectors.toList());
        globalMacAddress = device.globalMacAddress;
        localMacAddresses = device.localMacAddresses.stream().collect(Collectors.toList());
        timestamps = device.timestamps.stream().collect(Collectors.toList());
        points = device.points.stream().collect(Collectors.toList());
        router = device.router;
        rssis = device.rssis.stream().collect(Collectors.toList());
        ssids = device.ssids.stream().collect(Collectors.toList());
        expired = device.expired;
    }


    public String getMacAddress() {
        return globalMacAddress != null ? globalMacAddress :
                localMacAddresses != null && !localMacAddresses.isEmpty() ? localMacAddresses.get(0) : null;
    }

    boolean unique() {
        return globalMacAddress != null;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public List<String> getFootprints() {
        return footprints;
    }

    public void setFootprints(List<String> footprints) {
        this.footprints = footprints;
    }

    public String getGlobalMacAddress() {
        return globalMacAddress;
    }

    public void setGlobalMacAddress(String globalMacAddress) {
        this.globalMacAddress = globalMacAddress;
    }

    public List<String> getLocalMacAddresses() {
        return localMacAddresses;
    }

    public void setLocalMacAddresses(List<String> localMacAddresses) {
        this.localMacAddresses = localMacAddresses;
    }

    public List<Long> getTimestamps() {
        return timestamps;
    }

    public void setTimestamps(List<Long> timestamps) {
        this.timestamps = timestamps;
    }

    public List<GeoPoint> getPoints() {
        return points;
    }

    public void setPoints(List<GeoPoint> points) {
        this.points = points;
    }

    public String getRouter() {
        return router;
    }

    public void setRouter(String router) {
        this.router = router;
    }

    public List<Integer> getRssis() {
        return rssis;
    }

    public void setRssis(List<Integer> rssis) {
        this.rssis = rssis;
    }

    public List<String> getSsids() {
        return ssids;
    }

    public void setSsids(List<String> ssids) {
        this.ssids = ssids;
    }

    public boolean isExpired() {
        return expired;
    }

    public void setExpired(boolean expired) {
        this.expired = expired;
    }

    @Override
    public String toString() {
        return "Device{" +
                "_id='" + getMacAddress() + '\'' +
                ", counter=" + counter +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}
