package it.polito.wifidata.datafiltering.model;

import java.util.Date;

public class Variation {
    private long timestamp;
    private boolean descent;

    public Variation(long timestamp, boolean descent) {
        this.timestamp = timestamp;
        this.descent = descent;
    }

    public boolean isDescent() {
        return descent;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        if (descent)
            return "Descent at " + new Date(timestamp);
        return "Ascent at " + new Date(timestamp);
    }
}
