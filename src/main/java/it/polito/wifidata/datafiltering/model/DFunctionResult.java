package it.polito.wifidata.datafiltering.model;

public class DFunctionResult {
    private final int seqNumber;
    private final Double distance;

    public DFunctionResult(int seqNumber, Double distance) {
        this.seqNumber = seqNumber;
        this.distance = distance;
    }

    public int getSeqNumber() {
        return seqNumber;
    }

    public Double getDistance() {
        return distance;
    }
}
