package it.polito.wifidata.datafiltering.model;

import it.polito.wifidata.datafiltering.busstopsapi.pojo.Stop;

import java.util.Date;

public class StopsTableEntry {
    private String routerId;
    private Integer lineId;
    private Integer itineraryId;
    private Stop stop;
    private Long timestamp;
    private int numOfAscends;
    private int numOfDescends;
    private int devicesOnBoard;
    private Double matchDistance;
    private int segmentNumber; // number indicating to which time window this entry was created

    public StopsTableEntry(String routerId,
                           Integer lineId,
                           Integer itineraryId,
                           Stop stop,
                           Long timestamp,
                           Double matchDistance,
                           int segmentNumber) {
        this.routerId = routerId;
        this.lineId = lineId;
        this.itineraryId = itineraryId;
        this.stop = stop;
        this.timestamp = timestamp;
        this.matchDistance = matchDistance;
        this.segmentNumber = segmentNumber;
        this.numOfDescends = this.numOfAscends = 0;
    }

    public String getRouterId() {
        return routerId;
    }

    public Integer getLineId() {
        return lineId;
    }

    public Integer getItineraryId() {
        return itineraryId;
    }

    public Stop getStop() {
        return stop;
    }

    public void setStop(Stop stop) {
        this.stop = stop;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Double getMatchDistance() {
        return matchDistance;
    }

    public void setMatchDistance(Double matchDistance) {
        this.matchDistance = matchDistance;
    }

    public int getSegmentNumber() {
        return segmentNumber;
    }

    public void setSegmentNumber(int segmentNumber) {
        this.segmentNumber = segmentNumber;
    }

    public void descent() {
        numOfDescends++;
    }

    public void ascent() {
        numOfAscends++;
    }

    public void setNumOfDeviceOnBoard(int previousValue, Long previousTimestamp) {
        if (previousTimestamp != null &&
                Math.abs(timestamp - previousTimestamp) < 30 * 60 * 1000) {
            devicesOnBoard = previousValue + numOfAscends - numOfDescends;
            if (devicesOnBoard < 0)
                devicesOnBoard = 0;
        } else
            devicesOnBoard = numOfAscends;
    }

    @Override
    public String toString() {
        return "StopsTableEntry{" +
                "routerId='" + routerId + '\'' +
                ", lineId=" + lineId +
                ", itineraryId=" + itineraryId +
                ", stop=[" + Integer.toString(stop.getId()) + "] " + stop.getName() +
                ", timestamp=" + new Date(timestamp) +
                ", num of Devices=" + devicesOnBoard +
                ", num of Ascends=" + numOfAscends +
                ", num of Descends=" + numOfDescends +
                '}';
    }

    public int getNumOfAscents() {
        return numOfAscends;
    }

    public int getNumOfDescends() {
        return numOfDescends;
    }

    public int getDevicesOnBoard() {
        return devicesOnBoard;
    }
}
