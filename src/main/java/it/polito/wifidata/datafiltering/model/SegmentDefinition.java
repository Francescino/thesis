package it.polito.wifidata.datafiltering.model;

import java.util.Date;

public class SegmentDefinition {
    private Long startTimestamp;
    private Long endTimestamp;

    public SegmentDefinition(Long startTimestamp) {
        this.startTimestamp = startTimestamp;
        this.endTimestamp = null;
    }


    public Long getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(Long startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public Long getEndTimestamp() {
        return endTimestamp;
    }

    public void setEndTimestamp(Long endTimestamp) {
        this.endTimestamp = endTimestamp;
    }

    @Override
    public String toString() {
        return new Date(this.startTimestamp) + "-" + new Date(this.endTimestamp);
    }
}
