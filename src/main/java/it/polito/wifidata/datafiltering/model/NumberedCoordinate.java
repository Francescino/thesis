package it.polito.wifidata.datafiltering.model;

public class NumberedCoordinate {
    private int sequenceNumber;
    private GeoPoint coordinate;

    public NumberedCoordinate(int sequenceNumber, GeoPoint coordinate) {
        this.sequenceNumber = sequenceNumber;
        this.coordinate = coordinate;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public GeoPoint getCoordinate() {
        return coordinate;
    }
}
