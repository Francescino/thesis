package it.polito.wifidata.datafiltering.model;

import it.polito.wifidata.datafiltering.Config;

import java.util.*;

public class BusPath {
    private Map<Long, GeoPoint> timestampPoints;
    private List<SegmentDefinition> segments = new ArrayList<>();

    public BusPath(List<Position> positions,
                   String routerId,
                   Long startTime,
                   Long endTime) {
        positions.sort(new Comparator<Position>() {
            @Override
            public int compare(Position o1, Position o2) {
                return (int) (o1.getTimestamp() - o2.getTimestamp());
            }
        });
        timestampPoints = new TreeMap<>();
        // strip first part because it's deposit
        int index = 0;
        for (int i = 0; i < positions.size(); i++) {
            if (positions.get(i).getSpeed() >
                    Config.getInstance()
                            .getDouble("BUS_PATH_INITIAL_SPEED_THRESHOLD")) {
                index = i;
                break;
            }
        }
        if (!positions.isEmpty()) {
            List<Position> positions1 = positions.subList(index, positions.size() - 1);
            for (Position position : positions1) {
                if (position.getImei().equals(routerId)) {
                    timestampPoints.put(position.getTimestamp(),
                            new GeoPoint(position.getLat(),
                                    position.getLng()));
                }
            }
        }
    }


    public Iterator<List<TimePoint>> getSegments() {
        int numOfSegments = 0;
        Long first = null;
        for (Long aLong : timestampPoints.keySet()) {
            if (first == null) {
                first = aLong;
            } else if (aLong - first >= Config.getInstance()
                    .getLong("BUS_PATH_SEGMENT_TIME_WINDOW_WIDTH")) {
                numOfSegments++;
                first = null;
            }
        }
        int finalNumOfSegments = numOfSegments;
        return new Iterator<List<TimePoint>>() {
            int curSegment = 0;
            Long lastTimestamp;

            @Override
            public boolean hasNext() {
                return curSegment < finalNumOfSegments;
            }

            @Override
            public List<TimePoint> next() {
                List<TimePoint> res = new ArrayList<>();
                // start from the beggining
                Long first = null;
                for (Map.Entry<Long, GeoPoint> timePoint : timestampPoints.entrySet()) {
                    if (lastTimestamp != null && timePoint.getKey() <= lastTimestamp) continue;
                    if (first == null) {
                        first = timePoint.getKey();
                        res.add(new TimePoint(timePoint.getValue(),
                                timePoint.getKey()));
                        lastTimestamp = timePoint.getKey();
                    } else if (timePoint.getKey() - first <
                            Config.getInstance()
                                    .getLong("BUS_PATH_SEGMENT_TIME_WINDOW_WIDTH")) {
                        res.add(new TimePoint(timePoint.getValue(),
                                timePoint.getKey()));
                        lastTimestamp = timePoint.getKey();
                    } else
                        break;
                }
                curSegment++;
                return res;
            }
        };
    }

    private int findIndex(List<Long> longs, Long key) {
        int s, m, e;
        s = 0;
        e = longs.size() - 1;
        while (s != e && s < longs.size() && e >= 0) {
            if (longs.get(s).equals(key))
                return s;
            if (longs.get(e).equals(key))
                return e;
            m = (s + e) / 2;
            if (longs.get(m).equals(key))
                return m;
            if (longs.get(m) < key)
                s = m + 1;
            else
                e = m - 1;

        }
        return -1;
    }

    public Set<Map.Entry<Long, GeoPoint>> getPath() {
        return timestampPoints.entrySet();
    }


    public Iterator<List<TimePoint>> getCustomSegments(List<SegmentDefinition> segmentDefinitions) {
        return new Iterator<List<TimePoint>>() {
            int curIndex = 0;
            List<Long> timestamps = initTimestamp();
            List<GeoPoint> geoPoints = initGeoPoints();

            private List<Long> initTimestamp() {
                return new ArrayList<>(timestampPoints.keySet());
            }

            private List<GeoPoint> initGeoPoints() {
                return new ArrayList<>(timestampPoints.values());
            }


            @Override
            public boolean hasNext() {
                return curIndex < segmentDefinitions.size();
            }

            @Override
            public List<TimePoint> next() {
                SegmentDefinition segmentDefinition = segmentDefinitions.get(curIndex);
                int i1, i2;

                i1 = findIndex(timestamps, segmentDefinition.getStartTimestamp());
                i2 = findIndex(timestamps, segmentDefinition.getEndTimestamp());
                if (i1 > i2) // weird bug // TODO check
                {
                    int temp = i1;
                    i1 = i2;
                    i2 = temp;
                }
                List<Long> longs = timestamps.subList(i1, i2 - 1);
                List<GeoPoint> geoPoints = this.geoPoints.subList(i1, i2 - 1);
                List<TimePoint> res = new ArrayList<>();
                for (int i = 0; i < longs.size(); i++) {
                    res.add(new TimePoint(geoPoints.get(i), longs.get(i)));
                }
                curIndex++;
                return res;
            }
        };
    }
}
