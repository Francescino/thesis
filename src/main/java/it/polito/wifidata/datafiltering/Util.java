package it.polito.wifidata.datafiltering;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import it.polito.wifidata.datafiltering.busstopsapi.pojo.BusLineData;

import java.util.List;

public class Util {
    public static BusLineData getBusLine(List<BusLineData> lines, Integer lineId) {
        for (BusLineData line : lines) {
            if (line.getLineId().equals(lineId))
                return line;
        }
        return null;
    }

    public static boolean collectionExists(MongoDatabase database, final String collectionName) {
        MongoIterable<String> collectionNames = database.listCollectionNames();
        for (final String name : collectionNames) {
            if (name.equalsIgnoreCase(collectionName)) {
                return true;
            }
        }
        return false;
    }

    public static boolean databaseExist(MongoClient mongoClient, String dbName) {
        for (String listDatabaseName : mongoClient.listDatabaseNames()) {
            if (listDatabaseName.equals(dbName))
                return true;
        }
        return false;
    }
}
