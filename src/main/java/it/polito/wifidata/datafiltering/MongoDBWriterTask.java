package it.polito.wifidata.datafiltering;

import it.polito.wifidata.MongoClientSingleton;
import it.polito.wifidata.datafiltering.busstopsapi.LineManager;
import it.polito.wifidata.datafiltering.busstopsapi.pojo.BusLineData;
import it.polito.wifidata.datafiltering.busstopsapi.pojo.Itinerary;
import it.polito.wifidata.datafiltering.model.*;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Logger;

public class MongoDBWriterTask extends TimerTask {

    private final Logger LOGGER = Logger.getLogger(MyLogger.class.getName());
    private MobyClient mobyClient;

    public MongoDBWriterTask() {
        mobyClient = new MobyClient();
    }

    public static void main(String[] argv) {
        MongoDBWriterTask mongoWriterTask = new MongoDBWriterTask();
        mongoWriterTask.run();
        MongoClientSingleton.getInstance().close();
    }

    @Override
    public void run() {
        int decrement = Config.getInstance()
                .getInt("DECREMENT"); // control variable for selecting the day to analize

        // yesterday
        long startDate, endDate;
        Calendar date = new GregorianCalendar();
        // reset hour, minutes, seconds and millis
        date.add(Calendar.DATE, decrement);
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        startDate = date.getTimeInMillis();
        date.set(Calendar.HOUR_OF_DAY, 23);
        date.set(Calendar.MINUTE, 59);
        date.set(Calendar.SECOND, 59);
        date.set(Calendar.MILLISECOND, 999);
        endDate = date.getTimeInMillis();

        LOGGER.info("Starting data analysis from " +
                new Date(startDate) +
                " " +
                new Date(endDate));

        // get routers added in the system
        List<String> routerIds = LineManager.getRegisteredRoutersId();
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        // multi threading model for now disabled for accuracy
        for (String routerId : routerIds) {
            List<Position> rawPositions = getPositions(routerId,
                    startDate,
                    endDate);

            List<Device> groupedDevices = mobyClient.getGroupedDevices(routerId,
                    startDate,
                    endDate);

            // don't start anything if you don't have anything to examine
            if (rawPositions.isEmpty() ||
                    groupedDevices == null ||
                    groupedDevices.isEmpty()) {
                LOGGER.info(
                        "No data available for router with ID: " + routerId);
                continue;
            }

            // build bus path using positions
            BusPath busPath = new BusPath(rawPositions,
                    routerId,
                    startDate,
                    endDate);

            // filter devices
            FilterJob filterJob = new FilterJob(groupedDevices);
            // tell filter thread to exclude all devices detected at the deposit
            filterJob.setFirstValidTimestamp(busPath.getPath().iterator().next().getKey());
            Future<List<Variation>> variations = executorService.submit(filterJob);
            // get bus line paths
            List<BusLineData> linePaths = LineManager.getLinePaths(routerId);
            loadItinerariesInMongoDB(linePaths);
            LOGGER.info("Itineraries loaded in db.");
            List<SegmentDefinition> segmentDefinitions = getSegmentDefinitions(busPath,
                    startDate,
                    endDate);
            LOGGER.info("Path segmentated.");
            List<Future<SegmentClassification>> classifications = submitItineraryMatcherTask(executorService,
                    routerId,
                    linePaths,
                    busPath.getCustomSegments(segmentDefinitions));
            LOGGER.info(
                    "Itinerary matcher task submitted.");
            submitAndWaitStopMatcherJobs(executorService,
                    classifications);
            LOGGER.info(
                    "Stop table created.");
            // get result of filter job
            List<Variation> ascendAndDescends = null;
            try {
                ascendAndDescends = variations.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

            // save results
            StopsTable stopsTable = StopsTable.getInstance();
            if (ascendAndDescends != null &&
                    stopsTable != null &&
                    !stopsTable.getEntries().isEmpty()) {
                stopsTable.consumeAscendAndDescends(ascendAndDescends);
                LOGGER.info(
                        "Stops table:");
                for (StopsTableEntry entry : stopsTable.getEntries()) {
                    LOGGER.info(
                            entry.toString());
                }
                LOGGER.info(
                        "Saving data");
                MongoDbWriterJob mongoDbWriterJob = new MongoDbWriterJob(stopsTable);
                mongoDbWriterJob.run();
            }

        }
        executorService.shutdown();
    }

    private void submitAndWaitStopMatcherJobs(ExecutorService executorService,
                                              Iterable<? extends Future<SegmentClassification>> classifications) {
        StopsTable.newInstance();
        List<Future> stopsThread = new ArrayList<>();
        for (Future<SegmentClassification> classification : classifications) {
            try {
                SegmentClassification segmentClassification = classification.get();
                if (segmentClassification != null) {
                    Future<?> submit = executorService.submit(new StopMatcherJob(segmentClassification));
                    stopsThread.add(submit);
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        // wait stops thread
        for (Future future : stopsThread) {
            try {
                future.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    private List<Future<SegmentClassification>> submitItineraryMatcherTask(ExecutorService executorService,
                                                                           String routerId,
                                                                           List<BusLineData> linePaths,
                                                                           Iterator<List<TimePoint>> segmentsIterator) {
        int segmentNumber = -1;
        List<Future<SegmentClassification>> classifications = new ArrayList<>();
        while (segmentsIterator.hasNext()) {
            List<TimePoint> curSegment = segmentsIterator.next();
            segmentNumber++;
            if (segmentNumber > 0)
                classifications.add(executorService.submit(
                        new ItineraryMatcherJob(
                                routerId,
                                linePaths,
                                segmentNumber,
                                curSegment,
                                classifications.get(segmentNumber - 1)
                        )));
            else
                classifications.add(executorService.submit(
                        new ItineraryMatcherJob(
                                routerId,
                                linePaths,
                                segmentNumber,
                                curSegment,
                                null
                        )));
        }
        return classifications;
    }

    private List<SegmentDefinition> getSegmentDefinitions(BusPath busPath,
                                                          Long startDate,
                                                          Long endDate) {
        // find candidates start point for segmentation
        List<SegmentDefinition> coarseDefinitions = new ArrayList<>();
        Long start = startDate + Config.getInstance()
                .getLong("BUS_PATH_SEGMENT_START_HOUR");
        while (start < endDate) {
            SegmentDefinition coarseDefinition = new SegmentDefinition(start);
            start += Config.getInstance()
                    .getLong("BUS_PATH_SEGMENT_TIME_WINDOW_WIDTH");
            coarseDefinition.setEndTimestamp(start);
            coarseDefinitions.add(coarseDefinition);
        }

        List<SegmentDefinition> realDefinitions = new ArrayList<>();
        for (int i = 0; i < coarseDefinitions.size(); i++) {
            realDefinitions.add(new SegmentDefinition(null));
        }

        for (Map.Entry<Long, GeoPoint> timePoint : busPath.getPath()) {
            int i;
            for (i = 0; i < realDefinitions.size(); i++) {
                if (coarseDefinitions.get(i).getStartTimestamp() <= timePoint.getKey() &&
                        coarseDefinitions.get(i).getEndTimestamp() > timePoint.getKey())
                    break;
            }
            if (i < realDefinitions.size()) {
                if (realDefinitions.get(i).getStartTimestamp() == null ||
                        realDefinitions.get(i).getStartTimestamp() > timePoint.getKey())
                    realDefinitions.get(i).setStartTimestamp(timePoint.getKey());
                if (realDefinitions.get(i).getEndTimestamp() == null ||
                        realDefinitions.get(i).getEndTimestamp() < timePoint.getKey())
                    realDefinitions.get(i).setEndTimestamp(timePoint.getKey());
            }
        }

        realDefinitions.removeIf(segmentDefinition ->
                segmentDefinition.getStartTimestamp() == null ||
                        segmentDefinition.getEndTimestamp() == null ||
                        segmentDefinition.getStartTimestamp().equals(segmentDefinition.getEndTimestamp()));

        return realDefinitions;
    }

    private List<Position> getPositions(String routerId,
                                        Long startDate,
                                        Long endDate) {
        // recover and aggregate positions of router
        List<Position> rawPositions = new ArrayList<>();
        Iterator<Position> positions1 = mobyClient.getPositions(startDate,
                endDate);
        while (positions1.hasNext()) {
            Position next = positions1.next();
            //System.out.println(new Date(next.getTimestamp()) + " " + next.getSpeed());
            if (next != null && next.getImei().equals(routerId)) {
                rawPositions.add(next);
            }
        }
        return rawPositions;
    }

    private void loadItinerariesInMongoDB(List<BusLineData> linePaths) {
        // load bus data into mongodb
        MongoDBItineraryMatcher itineraryMatcher = new MongoDBItineraryMatcher();
        for (BusLineData linePath : linePaths) {
            for (Itinerary itinerary : linePath.getItineraries()) {
                itineraryMatcher.loadItinerary(linePath,
                        itinerary);
            }
        }
    }
}

                   /* List<Pair<Integer, Integer>> bestLines = new ArrayList<>(); // [(lineId, ItineraryId)]

                    // for each point of the segment
                    for (Pair<Long, GeoPoint> longGeoPointEntry : curSegment) {
                        lineProbability.setTimestamp(longGeoPointEntry.getKey());
                        int itineraryIndex = 0;
                        Pair<Integer, Double> resultDFunction;
                        double newProbability;
                        // match it to bus line itineraries
                        for (BusLineData linePath : allPossiblePaths) {
                            for (Itinerary itinerary : linePath.getItineraries()) {
                                resultDFunction = mongoDBItineraryMatcher.DFunction(linePath,
                                        itinerary.getId(),
                                        longGeoPointEntry.getValue(),
                                        lineProbability.getIndex(itineraryIndex));
                                //itinerary.DFunction(longGeoPointEntry.getValue(), lineProbability.getIndex(Integer.toString(itinerary.getId())));
                                newProbability = resultDFunction == null ? 0 :
                                        a * lineProbability.getProbability(itineraryIndex) +
                                                (1 - a) * (1000 / (999 + Math.exp(resultDFunction.getValue() / 3)));
                                lineProbability.setProbability(itineraryIndex++,
                                        newProbability,
                                        resultDFunction == null ? 0 : resultDFunction.getKey());
                            /*    lineProbability.setProbability(Integer.toString(itinerary.getId()),
                                        newProbability,
                                        resultDFunction == null? 0: resultDFunction.getKey());*//*
                            }
                        }*/
// take best probability and mark timestamp belonging to that line-itinerary
                     /*   int[] bestLineItineraryId = lineProbability.getMostProbableLineItineraryId();
                        if (bestLineItineraryId[0] >= 0 && bestLineItineraryId[1] >= 0) {
                            bestLines.add(new Pair<>(bestLineItineraryId[0], bestLineItineraryId[1]));
                        }
                    }*/

// calculate occurences to discover which line-itinerary appears the most in the current segment
                  /*  Integer bestItineraryId = null;
                    if (bestLines.size() > 1) {
                        Map<Integer, Integer> occurences = new HashMap<>();
                        for (Pair<Integer, Integer> bestLine : bestLines) {
                            occurences.put(bestLine.getValue(), occurences.containsKey(bestLine.getValue()) ?
                                    occurences.get(bestLine.getValue()) + 1 : 0);
                        }
                        Integer maxFreq = null;
                        for (Map.Entry<Integer, Integer> occurrence : occurences.entrySet()) {
                            if (bestItineraryId == null || occurrence.getValue() > maxFreq) {
                                bestItineraryId = occurrence.getKey();
                                maxFreq = occurrence.getValue();
                            }
                        }
                    } else if (!bestLines.isEmpty()) {
                        bestItineraryId = bestLines.get(0).getValue();
                    }
                    // now calculate the stop included in this time window
                    if (bestItineraryId != null) {
                        BusLineData selectedLine = null;
                        for (BusLineData linePath : allPossiblePaths) {
                            if (linePath.getItinerary(bestItineraryId) != null) {
                                selectedLine = linePath;
                                break;
                            }
                        }
                        if (selectedLine != null) {
                            GeoPoint stopGeoPoint = new GeoPoint();
                            Stop nearestStop;
                            for (Pair<Long, GeoPoint> timePoint : curSegment) {
                                nearestStop = mongoDBItineraryMatcher.getNearestStop(selectedLine,
                                        bestItineraryId,
                                        timePoint.getValue());
                                        /*selectedLine.getNearestStop(timePoint.getValue(),
                                        bestItineraryId,
                                        10.);*//*
                                if (nearestStop != null) {
                                    stopGeoPoint.setLat(nearestStop.getLatitude());
                                    stopGeoPoint.setLng(nearestStop.getLongitude());
                                    stopsTable.addEntry(routerId,
                                            selectedLine.getLineId(),
                                            bestItineraryId,
                                            nearestStop,
                                            timePoint.getKey(),
                                            timePoint.getValue().harvesineDistance(stopGeoPoint),
                                            segmentNumber);
                                }
                            }
                        }
                    }
                }*/
               /* stopsTable.sort();
                List<StopsTableEntry> entries = stopsTable.getEntries();*/
/*
                Random rand = new Random();
                HashMap<Integer, Color> colorMap = new HashMap<>();
                for (StopsTableEntry entry : entries) {
                    if (!colorMap.containsKey(entry.getLineId()))
                        colorMap.put(entry.getLineId(), new Color(rand.nextFloat(), rand.nextFloat(), rand.nextFloat()));
                    Stop stop = entry.getStop();
                    Color color = colorMap.get(entry.getLineId());
                    System.out.println(String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue()) + "," + stop.getLatitude() + ", " + stop.getLongitude() + ", " + entry.getLineId() + "-" + stop.getName() + "-" + new Date(entry.getTimestamp()));
                }*/
// System.out.println(); // TODO save stops table for that router
// wait for filter to compute final res then send it to mongo db saver thread
            /*        try {
                        filterThread.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    List<Device> filteredDevices = filterThread.getNonFilteredDevices();
                // merge stops and devices revelations
                if (filteredDevices != null) {
                    String currentDeviceId = null;
                    List<Device> currentGroup = new ArrayList<>();
                    for (Device device : filteredDevices) {
                        if (currentDeviceId == null) {
                            currentGroup.add(device);
                            currentDeviceId = device.getMacAddress();
                            System.out.println(currentDeviceId);
                            System.out.println("    from " + new Date(device.getStartTime()));
                            System.out.println("    to   " + new Date(device.getEndTime()));
                            continue;
                        }
                        if (!currentDeviceId.equals(device.getMacAddress())) {
                            stopsTable.consumeDevice(currentGroup);
                            currentDeviceId = device.getMacAddress();
                            System.out.println(currentDeviceId);
                            System.out.println("    from " + new Date(device.getStartTime()));
                            System.out.println("    to   " + new Date(device.getEndTime()));
                            currentGroup.clear();
                            currentGroup.add(device);
                        } else {
                            System.out.println("    from " + new Date(device.getStartTime()));
                            System.out.println("    to   " + new Date(device.getEndTime()));
                            currentGroup.add(device);
                        }
                    }

                    for (StopsTableEntry entry : entries) {
                        System.out.println(entry);
                    }

                    stopsTable.setEntries(new ArrayList<>());*/
