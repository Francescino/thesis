package it.polito.wifidata.datafiltering;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import it.polito.wifidata.MongoClientSingleton;
import it.polito.wifidata.datafiltering.busstopsapi.pojo.Stop;
import it.polito.wifidata.datafiltering.model.StopsTable;
import it.polito.wifidata.datafiltering.model.StopsTableEntry;
import org.bson.Document;

import java.util.*;

public class MongoDbWriterJob implements Runnable {

    private static final String CLASSIFICATION_RESULT_COLLECTION_NAME = "stops_table";
    private final StopsTable toLoad;
    private MongoClient mongoClient;

    public MongoDbWriterJob(StopsTable toLoad) {
        mongoClient = MongoClientSingleton.getInstance();
        this.toLoad = toLoad;
    }


    public synchronized boolean collectionExists(MongoDatabase database, final String collectionName) {
        MongoIterable<String> collectionNames = database.listCollectionNames();
        for (final String name : collectionNames) {
            if (name.equalsIgnoreCase(collectionName)) {
                return true;
            }
        }
        return false;
    }


    @Override
    public void run() {
        // save all the entries into mongo db
        Map<String, MongoCollection<Document>> collectionMap = new HashMap<>();
        MongoDatabase db = mongoClient.getDatabase(CLASSIFICATION_RESULT_COLLECTION_NAME);
        List<StopsTableEntry> entries = toLoad.getEntries();
        Document cur;
        Calendar hourOfDay = GregorianCalendar.getInstance();
        MongoCollection<Document> curCollection;
        String key;
        for (StopsTableEntry entry : entries) {
            key = Integer.toString(entry.getLineId());
            // find the collection
            if (collectionMap.containsKey(Integer.toString(entry.getLineId()))) {
                curCollection = collectionMap.get(key);
            } else {
                if (!collectionExists(db, key))
                    db.createCollection(key);
                collectionMap.put(key, db.getCollection(key));
                curCollection = collectionMap.get(key);
            }
            cur = new Document();
            cur.put("timestamp", entry.getTimestamp());
            hourOfDay.setTimeInMillis(entry.getTimestamp());
            cur.put("hour_of_day", hourOfDay.get(Calendar.HOUR_OF_DAY));
            cur.put("itinerary_id", entry.getItineraryId());
            cur.put("num_of_ascents", entry.getNumOfAscents());
            cur.put("num_of_descents", entry.getNumOfDescends());
            cur.put("num_of_devices_onboard", entry.getDevicesOnBoard());
            // save stop data
            Stop stop = entry.getStop();
            BasicDBObject stopObject = new BasicDBObject();
            BasicDBObject localityObject = new BasicDBObject();
            stopObject.put("id", stop.getId());
            stopObject.put("name", stop.getName());
            stopObject.put("lat", stop.getLatitude());
            stopObject.put("lng", stop.getLongitude());
            // build locality object
            localityObject.put("id", stop.getLocality().getId());
            localityObject.put("name", stop.getLocality().getName());
            localityObject.put("lat", stop.getLocality().getLatitude());
            localityObject.put("lng", stop.getLocality().getLongitude());
            stopObject.put("locality", localityObject);
            cur.put("stop", stopObject);
            curCollection.insertOne(cur);
        }
    }
}

