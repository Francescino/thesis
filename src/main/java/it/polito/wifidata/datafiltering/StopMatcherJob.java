package it.polito.wifidata.datafiltering;

import it.polito.wifidata.datafiltering.busstopsapi.pojo.Stop;
import it.polito.wifidata.datafiltering.model.GeoPoint;
import it.polito.wifidata.datafiltering.model.SegmentClassification;
import it.polito.wifidata.datafiltering.model.StopsTable;
import it.polito.wifidata.datafiltering.model.TimePoint;

import java.util.logging.Level;
import java.util.logging.Logger;

public class StopMatcherJob implements Runnable {
    private final SegmentClassification segmentClassification;
    private StopsTable globalStopsTable;
    private Logger LOGGER = Logger.getLogger(MyLogger.class.getName());

    public StopMatcherJob(SegmentClassification segmentClassification) {
        this.segmentClassification = segmentClassification;
        globalStopsTable = StopsTable.getInstance();
    }


    @Override
    public void run() {
        LOGGER.log(Level.INFO,
                segmentClassification.toString());
        MongoDBItineraryMatcher mongoDBItineraryMatcher = new MongoDBItineraryMatcher();
        Stop nearestStop;
        GeoPoint stopGeoPoint = new GeoPoint();
        for (TimePoint timePoint : segmentClassification.getSegment()) {
            nearestStop = mongoDBItineraryMatcher.getNearestStop(segmentClassification.getLineData(),
                    segmentClassification.getItineraryId(),
                    timePoint,
                    null);
            if (nearestStop != null) {
                stopGeoPoint.setLat(nearestStop.getLatitude());
                stopGeoPoint.setLng(nearestStop.getLongitude());
                globalStopsTable.addEntry(segmentClassification.getRouterId(),
                        segmentClassification.getLineData()
                                .getLineId(),
                        segmentClassification.getItineraryId(),
                        nearestStop,
                        timePoint.getTimestamp(),
                        timePoint.harvesineDistance(stopGeoPoint),
                        segmentClassification.getSegmentNumber());
            }
        }
    }
}
