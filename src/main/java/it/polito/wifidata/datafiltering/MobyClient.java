package it.polito.wifidata.datafiltering;

import it.polito.wifidata.datafiltering.model.Device;
import it.polito.wifidata.datafiltering.model.Position;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class MobyClient {
    private static String baseUrl = "https://my-moby.com/esp/";
    private final MobyService mobyService;

    public MobyClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mobyService = retrofit.create(MobyService.class);
    }

    public List<Device> getGroupedDevices(String routerId,
                                          Long startDate,
                                          Long endDate) {
        try {
            Response<List<Device>> response = mobyService.getDevices(routerId,
                    startDate,
                    endDate).execute();
            return response.body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Iterator<Position> getPositions(Long startDate,
                                           Long endDate) {
        return new Iterator<Position>() {
            boolean moreItems = true;
            int nPage = 0;
            int nItems = 10000;
            int currentIndex;
            List<Position> currentPage;

            @Override
            public boolean hasNext() {
                return moreItems;
            }

            private void getNextPage() throws IOException {
                currentPage = mobyService.getPositions(startDate,
                        endDate,
                        nPage,
                        nItems).execute().body();
                nPage++;
                currentIndex = 0;
                if (currentPage == null || currentPage.size() == 0)
                    moreItems = false;
            }

            @Override
            public Position next() {
                try {
                    if (currentPage == null)
                        getNextPage();
                    Position result;
                    if (currentPage != null && !currentPage.isEmpty())
                        result = currentPage.get(currentIndex);
                    else {
                        moreItems = false;
                        return null;
                    }
                    currentIndex++;
                    if (currentIndex >= currentPage.size())
                        getNextPage();
                    return result;
                } catch (IOException e) {
                    moreItems = false;
                }
                return null;
            }
        };
    }
}
