package it.polito.wifidata.datafiltering;

import com.mongodb.MongoClient;
import com.mongodb.MongoQueryException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Indexes;
import com.mongodb.client.model.geojson.Point;
import com.mongodb.client.model.geojson.Position;
import it.polito.wifidata.MongoClientSingleton;
import it.polito.wifidata.datafiltering.busstopsapi.pojo.BusLineData;
import it.polito.wifidata.datafiltering.busstopsapi.pojo.GeometryItem;
import it.polito.wifidata.datafiltering.busstopsapi.pojo.Itinerary;
import it.polito.wifidata.datafiltering.busstopsapi.pojo.Stop;
import it.polito.wifidata.datafiltering.model.DFunctionResult;
import it.polito.wifidata.datafiltering.model.GeoPoint;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.near;

public class MongoDBItineraryMatcher {
    private static final String ITINERARY_COORDINATES_DB_NAME = "itineraries";
    private static final String STOPS_DB_NAME = "stops";
    private static final String COORDINATE_FIELD = "coordinates";
    private static final String SEQ_NUM_FIELD = "sequence_number";
    private static final String STOP_ID_FIELD = "stop_id";


    private final MongoClient mongoClient;

    public MongoDBItineraryMatcher() {
        mongoClient = MongoClientSingleton.getInstance();
    }

    public Stop getNearestStop(BusLineData busLineData,
                               Integer itineraryId,
                               GeoPoint toMatch,
                               List<Integer> stopsSeqNum) {
        MongoCollection<Document> stopsCoordinateCollection = mongoClient.getDatabase(STOPS_DB_NAME)
                .getCollection(Integer.toString(itineraryId));
        Point refPoint = new Point(new Position(toMatch.getLat(),
                toMatch.getLng()));
        Bson filter;
        if (stopsSeqNum == null)
            filter = near(COORDINATE_FIELD,
                    refPoint,
                    Config.getInstance()
                            .getDouble("MONGODB_ITINERARY_MATCHER_STOP_MATCH_DISTANCE"),
                    0.);
        else {
            List<Bson> seqNumFilters = new ArrayList<>();
            for (Integer stopSeqNum : stopsSeqNum) {
                seqNumFilters.add(Filters.eq(SEQ_NUM_FIELD, stopSeqNum));
            }
            filter = Filters.and(
                    Filters.or(seqNumFilters),
                    near(COORDINATE_FIELD,
                            refPoint,
                            Config.getInstance()
                                    .getDouble("MONGODB_ITINERARY_MATCHER_STOP_MATCH_DISTANCE"),
                            0.)
            );
        }
        FindIterable<Document> documents;
        try {
            documents = stopsCoordinateCollection.find(filter);
        } catch (MongoQueryException e) {
            stopsCoordinateCollection.createIndex(Indexes.geo2dsphere(COORDINATE_FIELD));
            documents = stopsCoordinateCollection.find(filter);
        }
        Stop res = null;
        if (documents != null) {
            Document first = documents.first();
            if (first != null) {
                Integer stopId = first.getInteger(STOP_ID_FIELD);
                for (Stop stop : busLineData.getStops()) {
                    if (stop.getId().equals(stopId)) {
                        res = stop;
                        break;
                    }
                }
            }
        }

        return res;
    }

    public DFunctionResult DFunction(BusLineData busLine,
                                     Integer itineraryId,
                                     GeoPoint toMatch,
                                     Integer currentSequenceNumber) {
        MongoCollection<Document> documentMongoCollection = mongoClient.getDatabase(ITINERARY_COORDINATES_DB_NAME)
                .getCollection(Integer.toString(itineraryId));
        int sequenceNumber = currentSequenceNumber != null ? currentSequenceNumber : 0;
        Point refPoint = new Point(new Position(toMatch.getLat(),
                toMatch.getLng()));
        Bson filter = Filters.and(
                Filters.gt(SEQ_NUM_FIELD, sequenceNumber),
                near(COORDINATE_FIELD,
                        refPoint,
                        Config.getInstance()
                                .getDouble("MONGODB_ITINERARY_MATCHER_MATCH_DISTANCE"),
                        0.)
        );
        FindIterable<Document> documents;
        try {
            documents = documentMongoCollection.find(filter);
        } catch (MongoQueryException e) {
            documentMongoCollection.createIndex(Indexes.geo2dsphere(COORDINATE_FIELD));
            documents = documentMongoCollection.find(filter);
        }
        if (documents != null) {
            Document first = documents.first();
            if (first != null) {
                Document document = first.get(COORDINATE_FIELD, Document.class);
                ArrayList coordinates = document.get(COORDINATE_FIELD, ArrayList.class);
                GeoPoint readPoint = new GeoPoint((double) coordinates.get(0),
                        (double) coordinates.get(1));
                int seqNum = first.getInteger(SEQ_NUM_FIELD);
                return new DFunctionResult(seqNum, readPoint.harvesineDistance(toMatch));
            }
        }
        return null;
    }

    /**
     * checks if the itinerary exist inside mongo db
     * if not it loads it
     *
     * @param itinerary
     */
    void loadItinerary(BusLineData busLineData, Itinerary itinerary) {
        MongoDatabase coordinateDatabase = mongoClient.getDatabase(ITINERARY_COORDINATES_DB_NAME);
        MongoDatabase stopsDatabase = mongoClient.getDatabase(STOPS_DB_NAME);
        MongoCollection<Document> coordinateCollection = null;
        String itineraryId = Integer.toString(itinerary.getId());
        if (!Util.collectionExists(coordinateDatabase, itineraryId)) {
            int sequenceNumber = 0;
            coordinateDatabase.createCollection(itineraryId);
            stopsDatabase.createCollection(itineraryId);
            coordinateCollection = coordinateDatabase.getCollection(itineraryId);
            MongoCollection<Document> stopsCollection = stopsDatabase.getCollection(itineraryId);
            // create coordinates collection
            Document currentDocumentPoint = new Document();
            GeoPoint currentLinePoint;
            for (GeometryItem geometry : itinerary.getGeometries()) {
                // put departure
                currentDocumentPoint = new Document();
                currentDocumentPoint.put(COORDINATE_FIELD,
                        new Point(new Position(geometry.getDeparture().getLatitude(),
                                geometry.getDeparture().getLongitude())));
                currentDocumentPoint.put(SEQ_NUM_FIELD,
                        sequenceNumber);
                coordinateCollection.insertOne(currentDocumentPoint);
                sequenceNumber++;
                for (int i = 0; i < geometry.getGeometry().get(0).getCoordinates().size() - 1; i++) {
                    List<Double> firstCoordinate = geometry.getGeometry().get(0).getCoordinates().get(i);
                    List<Double> secondCoordinate = geometry.getGeometry().get(0).getCoordinates().get(i + 1);
                    GeoInterpolator geoInterpolator = new GeoInterpolator(firstCoordinate.get(1),
                            firstCoordinate.get(0),
                            secondCoordinate.get(1),
                            secondCoordinate.get(0),
                            Config.getInstance()
                                    .getInt("MONGODB_ITINERARY_MATCHER_INTERPOLATION_POINTS"));
                    while (geoInterpolator.hasNext()) {
                        currentDocumentPoint = new Document();
                        currentLinePoint = geoInterpolator.next();
                        currentDocumentPoint.put(COORDINATE_FIELD,
                                new Point(new Position(currentLinePoint.getLat(),
                                        currentLinePoint.getLng())));
                        currentDocumentPoint.put(SEQ_NUM_FIELD,
                                sequenceNumber);
                        coordinateCollection.insertOne(currentDocumentPoint);
                        sequenceNumber++;
                    }
                }
                // put arrival
                currentDocumentPoint = new Document();
                currentDocumentPoint.put(COORDINATE_FIELD,
                        new Point(new Position(geometry.getArrival().getLatitude(),
                                geometry.getArrival().getLongitude())));
                currentDocumentPoint.put(SEQ_NUM_FIELD,
                        sequenceNumber);
                coordinateCollection.insertOne(currentDocumentPoint);
                sequenceNumber++;
            }
            // create stops coordinate collection
            List<Stop> stopsByIds = busLineData.getStopsByIds(itinerary.getStopPointIds());
            Document currentDocument;
            int seqNum = 0;
            for (Stop stopsById : stopsByIds) {
                currentDocument = new Document();
                currentDocument.put(COORDINATE_FIELD,
                        new Point(new Position(stopsById.getLatitude(),
                                stopsById.getLongitude())));
                currentDocument.put(STOP_ID_FIELD,
                        stopsById.getId());
                currentDocument.put(SEQ_NUM_FIELD,
                        seqNum++);
                stopsCollection.insertOne(currentDocument);
            }
            if (coordinateCollection != null)
                coordinateCollection.createIndex(Indexes.geo2dsphere(COORDINATE_FIELD));
            if (stopsCollection != null)
                stopsCollection.createIndex(Indexes.geo2dsphere(COORDINATE_FIELD));
        }
    }


}
