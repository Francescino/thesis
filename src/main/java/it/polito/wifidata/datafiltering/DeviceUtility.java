package it.polito.wifidata.datafiltering;

import it.polito.wifidata.datafiltering.model.Device;
import it.polito.wifidata.datafiltering.model.GeoPoint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeviceUtility {

    public static List<Device> filterCameras(List<Device> devices) {
        if (devices == null || devices.isEmpty())
            return null;

        List<Device> validDevices = new ArrayList<>();
        Map<String, List<Device>> groups = new HashMap<>();
        List<Device> group;
        for (Device device : devices) {
            // make sets of devices with the same mac address
            // to check if they are validDevices
            group = groups.getOrDefault(device.getMacAddress(), null);
            if (group != null)
                group.add(device);
            else {
                groups.put(device.getMacAddress(), new ArrayList<>());
                groups.get(device.getMacAddress()).add(device);
            }
        }
        int validGroups = 0;
        int invalidGroups = groups.size();
        for (List<Device> grp : groups.values()) {
            if (validateDeviceDetections(grp)) {
                validDevices.addAll(grp);
                validGroups++;
            }
        }
        invalidGroups -= validGroups; // just for statistics reached from debug
        return validDevices;
    }

    /**
     * @param detections list of device with the same ID
     * @return if the input group are actually user device detections
     */

    private static boolean validateDeviceDetections(List<Device> detections) {
        if (detections == null ||
                detections.isEmpty())
            return false;

        int i, sum, totalMass;
        i = sum = totalMass = 0;
        Long firstTimestamp, endTimestamp;
        firstTimestamp = endTimestamp = null;
        GeoPoint center;
        Double totalX, totalY;
        totalX = totalY = 0.;
        for (Device detected : detections) {
            sum += detected.getCounter();

            // check continuity
            if (firstTimestamp == null) {
                firstTimestamp = detected.getStartTime();
                endTimestamp = detected.getEndTime();
            } else if (Math.abs(detected.getStartTime() - endTimestamp) < Config.getInstance()
                    .getLong("FILTER_LOCAL_TIME_WINDOW"))
                endTimestamp = detected.getEndTime();
            else
                firstTimestamp = endTimestamp = null;

            for (GeoPoint point : detected.getPoints()) {
                totalMass++;
                totalX += point.getLat();
                totalY += point.getLng();
            }
        }
        // RULE 2 not enough detections
        if (sum <= Config.getInstance().getInt("MIN_RELEVATIONS"))
            return false;

        // RULE 1 check center of mass
        // if this is nearby the first point then is this revelation is concentrated in one point
        if (totalMass > 1) {
            center = new GeoPoint(totalX / totalMass, totalY / totalMass);
            // meters
            Integer nearbyRadius = Config.getInstance().getInt("NEARBY_RADIUS");
            if (center.harvesineDistance(
                    detections.get(0)
                            .getPoints()
                            .get(0)) < nearbyRadius)
                return false;
        } else
            return false;


        // RULE 3 too much time on the bus
        return endTimestamp == null ||
                firstTimestamp == null ||
                endTimestamp - firstTimestamp <= Config.getInstance()
                        .getLong("MAX_DISTANCE_BETWEEN_TIME_INTERVALS");
    }

}
