package it.polito.wifidata.datafiltering;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import it.polito.wifidata.MongoClientSingleton;
import org.bson.Document;

import java.util.HashMap;
import java.util.Map;

public class Config {
    // matching algorithm params
    private static final String DB_NAME = "algoConfig";
    private static final String COLLECTION_NAME = "parameters";
    private static Config instance;
    private Map<String, Object> cache;

    private Config() {
        boolean found = false;
        MongoClient mongoClient = MongoClientSingleton.getInstance();
        if (!Util.databaseExist(mongoClient, DB_NAME))
            createDb(mongoClient);
    }

    public static Config getInstance() {
        if (instance == null)
            instance = new Config();
        return instance;
    }

    private void createDb(MongoClient mongoClient) {
        MongoDatabase database = mongoClient.getDatabase(DB_NAME);
        Document parameters = new Document();
        // Load standard constants
        parameters.put("MONGO_HOST", "localhost"); // TO EDIT CONFIG LOAD IN LOCAL THEN OVERRIDE WITH REMOTE DB
        parameters.put("DECREMENT", -1);
        parameters.put("LINE_PROBABILITY_THRESHOLD", 0.6);
        parameters.put("ITINERARY_MATCHER_RESET_TIME", 3600000); // s
        parameters.put("BUS_PATH_SEGMENT_START_HOUR", 18000000); // s
        parameters.put("D_FUNCTION_A_PARAMETER", 9. / 10.);
        parameters.put("BUS_PATH_SEGMENT_TIME_WINDOW_WIDTH", 10 * 60 * 1000); //min
        parameters.put("BUS_PATH_INITIAL_SPEED_THRESHOLD", (double) 5.0);
        parameters.put("MONGODB_INITNERARY_MATCHER_INTERPOLATION_POINTS", 50);
        parameters.put("MONGODB_ITINERARY_MATCHER_MATCH_DISTANCE", 50.);
        parameters.put("MONGODB_ITINERARY_MATCHER_STOP_MATCH_DISTANCE", 20.);
        parameters.put("MAX_DISTANCE_BETWEEN_TIME_INTERVALS", 3 * 3600 * 1000);
        parameters.put("MIN_RELEVATIONS", 2);
        parameters.put("NEARBY_RADIUS", 20);
        parameters.put("FILTER_LOCAL_TIME_WINDOW", 10 * 60 * 1000);
        parameters.put("LOGGER_LEVEL", "INFO");
        database.createCollection(COLLECTION_NAME);
        MongoCollection<Document> collection = database.getCollection("parameters");
        collection.insertOne(parameters);
    }

    public Object get(String key) {
        if (cache == null) cache = new HashMap<>();
        if (!cache.containsKey(key)) {
            MongoDatabase database = MongoClientSingleton.getInstance()
                    .getDatabase(DB_NAME);
            MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);
            FindIterable<Document> query = collection.find().limit(1);
            for (Document document : query) {
                for (Map.Entry<String, Object> stringObjectEntry : document.entrySet()) {
                    cache.put(stringObjectEntry.getKey(),
                            stringObjectEntry.getValue());
                }
            }
        }
        return cache.getOrDefault(key, null);
    }

    public Integer getInt(String key) {
        Object value;
        if ((value = get(key)) != null
                && value instanceof Integer) {
            return Integer.valueOf(value.toString());
        }
        return null;
    }

    public Long getLong(String key) {
        Object value;
        if ((value = get(key)) != null
                && (value instanceof Long || value instanceof Integer)) {
            return Long.valueOf(value.toString());
        }
        return null;
    }

    public Double getDouble(String key) {
        Object value;
        if ((value = get(key)) != null
                && value instanceof Double) {
            return Double.valueOf(value.toString());
        }
        return null;
    }


}
