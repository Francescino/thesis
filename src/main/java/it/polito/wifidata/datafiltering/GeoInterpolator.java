package it.polito.wifidata.datafiltering;

import it.polito.wifidata.datafiltering.model.GeoPoint;
import net.sf.geographiclib.Geodesic;
import net.sf.geographiclib.GeodesicData;
import net.sf.geographiclib.GeodesicLine;
import net.sf.geographiclib.GeodesicMask;

import java.util.Iterator;

public class GeoInterpolator implements Iterator<GeoPoint> {
    private Double step;
    private Double t;
    private Double distance;
    private GeodesicLine line;

    public GeoInterpolator(Double lat1, Double lng1, Double lat2, Double lng2, Integer numOfPoints) {
        step = 1. / numOfPoints;
        t = 0.;
        Geodesic geod = Geodesic.WGS84;
        line = geod.InverseLine(lat1,
                lng1,
                lat2,
                lng2,
                GeodesicMask.DISTANCE_IN |
                        GeodesicMask.LATITUDE |
                        GeodesicMask.LONGITUDE);
        distance = line.Distance();
    }

    @Override
    public boolean hasNext() {
        return t <= 1;
    }

    @Override
    public GeoPoint next() {
        GeodesicData g = line.Position(distance * t,
                GeodesicMask.LATITUDE |
                        GeodesicMask.LONGITUDE);
        t += step;
        return new GeoPoint(g.lat1, g.lon2);
    }
}
