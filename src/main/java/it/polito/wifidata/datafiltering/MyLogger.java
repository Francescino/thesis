package it.polito.wifidata.datafiltering;

import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;

public class MyLogger {

    public MyLogger() {
        Logger logger = Logger.getLogger(this.getClass().getName());
        logger.addHandler(new ConsoleHandler());
    }
}
