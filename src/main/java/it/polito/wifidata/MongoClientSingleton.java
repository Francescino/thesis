package it.polito.wifidata;

import com.mongodb.MongoClient;

public class MongoClientSingleton {
    protected static String mongoHost = "localhost"; // TODO create config file
    private static MongoClient instance = null;

    public static MongoClient getInstance() {
        if (instance == null) {
            instance = new MongoClient(mongoHost);
        }

        return instance;
    }
}
